'use strict';

$(document).ready(function () {
  ui.links.dummyLink(); //avoid page scrolling when an empty link is clicked
  ui.links.scrollTo(); //give a link the ability of scrolling to an anchor

  ui.elements.searchbar.dropdown(); //custom dropdown for searchbar
  ui.elements.featured.carsDetails(); //display more information for car blocs
  ui.elements.search.rangeSlider(); //custom dropdown for searchbar
  ui.elements.result.toggle(); //custom dropdown for searchbar
  ui.elements.header.affix(); //header affix

  ui.form.selectmenu(); //customize select elements
  ui.form.checkbox(); //customize checkbox elements

  ui.sliders.banner();

  ui.objects.matchHeight(); //give the same height to targeted objects
});

var ui = {
  links: {
    dummyLink: function dummyLink() {

      /*-- avoid page scrolling when an empty link is clicked --*/
      $('a[href="#"]').on('click', function (e) {
        e.preventDefault();
      });
      /*-- end: avoid page scrolling when an empty link is clicked --*/
    },
    scrollTo: function scrollTo() {

      /*-- give a link the ability of scrolling to an anchor --*/
      var $link = $('a.action-scrollTo');
      var $body = $('html, body');
      if ($link.length) {

        $link.on('click', function (e) {
          e.preventDefault();
          var $this = $(this);
          var target = $($this.attr('href')).offset().top;
          $body.animate({ 'scrollTop': target }, 750);
        });
        /*-- give a link the ability of scrolling to an anchor --*/
      }
    }
  },
  sliders: {
    banner: function banner() {
      var $slider = $('#banner #slider');
      if ($slider.length) {
        $slider.slick({
          slidesToShow: 3,
          slidesToScroll: 1,
          prevArrow: '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button"><i class="icon icon-arrow-left"></i></button>',
          nextArrow: '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button"><i class="icon icon-arrow-right"></i></button>',
          responsive: [{
            breakpoint: 1024,
            settings: {
              slidesToShow: 2
            }
          }, {
            breakpoint: 768,
            settings: {
              slidesToShow: 1
            }
          }]
        });
      }
    }
  },
  objects: {
    matchHeight: function matchHeight() {

      /*- give the same height to targeted elements --*/
      var $matchHeight = $('.matchHeight');
      if ($matchHeight.length) {
        $matchHeight.matchHeight();
      }
      /*- end: give the same height to targeted elements --*/
    }
  },
  form: {
    selectmenu: function selectmenu() {
      var $select = $('select.select');
      if ($select.length) {
        $select.each(function () {
          var $this = $(this);

          //import data-icon attribute value as icon class
          if (typeof $this.attr('data-icon') !== 'undefined') {
            var $icon = 'icon icon-' + $this.attr('data-icon');
          } else {
            var $icon = 'ui-icon-triangle-1-s';
          }

          $this.selectmenu({
            classes: {
              'ui-selectmenu-button': 'ui-selectmenu-button form-control'
            },
            icons: {
              button: $icon
            }
          });
        });
      }
    },
    checkbox: function checkbox() {
      var $checkbox = $('input[type="checkbox"].checkbox');
      if ($checkbox.length) {
        $checkbox.checkboxradio({
          classes: {
            'ui-checkboxradio-icon': 'icon icon-check'
          }
        });
      }
    }
  },
  elements: {
    header: {
      affix: function affix() {
        var $window = $(window);
        var $header = $('#header.section .section-header');
        if ($header.length) {
          var $page = $('#page');
          $window.on('scroll', function () {
            if ($window.scrollTop() > 0) {
              $page.addClass('pin');
            } else {
              $page.removeClass('pin');
            }
          });
        }
      }
    },
    search: {
      rangeSlider: function rangeSlider() {
        var $ranges = $('#range-budget, #range-distance');
        if ($($ranges.length)) {
          $('#range-budget').slider({
            range: true,
            min: 69,
            max: 600,
            values: [69, 600],
            slide: function slide(event, ui) {
              //$( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
              $range_budget_l.text(ui.values[0]);
              $range_budget_r.text(ui.values[1]);
            }
          });
          $('#range-distance').slider({
            range: true,
            min: 10000,
            max: 60000,
            values: [10000, 60000],
            slide: function slide(event, ui) {
              //$( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
              $range_distance_l.text(ui.values[0]);
              $range_distance_r.text(ui.values[1]);
            }
          });

          var $range_budget_l = $('#range-budget .handle-left .value');
          var $range_budget_r = $('#range-budget .handle-right .value');

          $range_budget_l.text($('#range-budget').slider('values', 0));
          $range_budget_r.text($('#range-budget').slider('values', 1));

          var $range_distance_l = $('#range-distance .handle-left .value');
          var $range_distance_r = $('#range-distance .handle-right .value');

          $range_distance_l.text($('#range-distance').slider('values', 0));
          $range_distance_r.text($('#range-distance').slider('values', 1));
        }
      }
    },
    result: {
      toggle: function toggle() {
        var $toggle = $('#result .result-header .btn');
        if ($toggle.length) {
          var $search = $('#result #search');
          var $result = $('#result');
          $toggle.on('click', function () {
            var $this = $(this);
            $search.slideToggle(function () {
              $result.toggleClass('active');
              $this.toggleClass('active');
            });
          });
        }
      }
    },
    searchbar: {
      dropdown: function dropdown() {
        var $dropdown = $('[id*="search"].search-form .form-group .dropdown .form-control');
        var $dropdown_menu = $('[id*="search"].search-form .form-group .dropdown .form-control + .dropdown-menu');
        if ($dropdown.length) {
          $dropdown.on('click', function (e) {
            e.stopPropagation();
            var $this = $(this);
            var $target = $this.next('.dropdown-menu');
            $dropdown_menu.removeClass('open');
            $target.toggleClass('open');
          });
          $dropdown_menu.on('click', function (e) {
            e.stopPropagation();
          });
          $(document).on('click', function () {
            $dropdown_menu.removeClass('open');
          });
        }
      }
    },
    featured: {
      carsDetails: function carsDetails() {
        var $bloc_car = $('#featured .featured-body .block-car');
        var $button = $('#featured .featured-body .block-car a');
        if ($bloc_car.length) {
          $button.on('click', function () {
            var $this = $(this);
            var $parent_block = $(this).closest('.block-car');
            $bloc_car.removeClass('active hover');
            $parent_block.addClass('active');
          }).on('mouseenter', function () {
            var $this = $(this);
            var $parent_block = $(this).closest('.block-car');
            $bloc_car.removeClass('hover');
            $parent_block.addClass('hover');
          }).on('mouseleave', function () {
            var $this = $(this);
            var $parent_block = $(this).closest('.block-car');
            $bloc_car.removeClass('hover');
          });
        }

        $bloc_car.on('click', function (e) {
          e.stopPropagation();
        });

        $(document).on('click', function () {
          $bloc_car.removeClass('active hover');
        });
      }
    }
  }
};
//# sourceMappingURL=main.js.map
