
$(document).ready(function(){
	
	$('.cmd-registration').hide();

    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }
	
	function isUniqueSiren( siren ){
		var ret = true;
		$.ajax({
			url: 'http://bo.loc-courtoisie.fr/admin/customer/check-siren-exist/?siren='+ siren +'&client=&from_other=1',
			dataType: "jsonp",
			jsonpCallback: 'apiStatus',
			type: 'GET'
		});
			
		return ret;
	}
	
	$('.creer_compte').click(function(){
		$('.cmd-registration').show();
		$('.txt-societe').focus();
	});
	
	$('.get_client').click(function(){
		if ( $('.input-sirene').val() == '' ) {
			alert('Code client ou sirène invalide');
		}else{
			$.ajax({
				url: 'http://www.loc-courtoisie.fr/ajax/reload_form_code.php?code='+$('.input-sirene').val()+'&from_other=1',
				dataType: "jsonp",
				jsonpCallback: 'apiStatus',
				type: 'GET',
				async: false,
				success: function (data) {
					if ( data.length == 0 )
					{ 
						alert("Client non trouvé");
					}else{
						$('.txt-societe').val(data.societe);
						$('.txt-adresse').val(data.adresse);
						$('.txt-code-postal').val(data.code_postal);
						$('.txt-ville').val(data.ville);
						$('.txt-rcs').val(data.siret);
						$('.txt-siren').val(data.siren);
						$('.txt-nom').val(data.nom);
						$('.txt-telephone').val(data.tel);
						$('.txt-fax').val(data.fax);
						$('.txt-email').val(data.email);
						
						if (data.id_civilite != null)
							$('.civilite').val(data.id_civilite);
						 
						$('.hnew').val(1);
						$('.cmd-registration').show();
						window.location.href = "#cmd-registration";
					}
					
				},
				error: function(xhr, ajaxOptions, thrownError) {

				}
			});
		}
	});
	
	//save commande
    $('.valider_commande').click(function(){
		$('.cmd-registration>div').removeClass("has-error");
		$('.cmd-registration>div').removeClass("has-feedback");
		$('.alert-danger').hide();
		
		window.location.href = "#cmd-registration";

		var ret = true;
        var zSociete = $(".txt-societe").val();
        var zAdresse = $(".txt-adresse").val();
        var zCp      = $(".txt-code-postal").val();
        var zVille   = $(".txt-ville").val();
        var zRcs     = $(".txt-rcs").val();
        var zSiren   = $(".txt-siren").val();
        var zNom     = $(".txt-nom").val();
		var zCivilite= $(".civilite").val();
        var zTelephone  = $(".txt-telephone").val();
        var zFax     = $(".txt-fax").val();
        var zEmail   = $(".txt-email").val();
        var zAdresseLivr   = $(".txt-adresse-livraison").val();
        var zCpLivr   = $(".txt-cp-livraison").val();
        var zVilleLivr= $(".txt-ville-livraison").val();
		
		var zDuree      = $('.duree').val().replace(' mois','');
        var zKm         = $('.km').val().replace(' km','');
        var zContrat    = $('.contrat').val();
        var zCouleur    = $('.couleur').val();
        var zQuantite   = $('.quantite').val();
        var zPrix       = $('.prix').val();
        var zVersion    = $('.version').val();
        var zExtension  = $('.id_extension').val();
        var zSitename   = $('.hsite_name').val();
        var iIdSite     = $('.hsite_id').val();

		var zPerte		= $('.subscribe_option:checked').val() != 'undefined' ? 1 : 0 ;


        if( zSociete == '' )
        {
            $(".txt-societe-error").show();
            var div = $(".txt-societe").closest("div").parent();
            div.addClass("has-error has-feedback");
            ret = false;
        }
        // else
        // {
            // $(".txt-societe-error").hide();
            // var div = $(".txt-societe").closest("div").parent();
            // div.removeClass("has-error");
            // ret = true;
        // }

        if( zAdresse == '' )
        {
            $(".txt-adresse-error").show();
            var div = $(".txt-adresse").closest("div").parent();
            div.addClass("has-error has-feedback");
            ret = false;
        }
        // else
        // {
            // $(".txt-adresse-error").hide();
            // var div = $(".txt-adresse").closest("div").parent();
            // div.removeClass("has-error");
            // ret = true;
        // }

        if(zCp == '' || zCp.length != 5)
        {
            $(".txt-code-postal-error").show();
            var div = $(".txt-code-postal").closest("div").parent();
            div.addClass("has-error has-feedback");
            ret = false;
        }
        // else
        // {
            // $(".txt-code-postal-error").hide();
            // var div = $(".txt-code-postal").closest("div").parent();
            // div.removeClass("has-error");
            // ret = true;
        // }

        if(zVille== '')
        {
            $(".txt-ville-error").show();
            var div = $(".txt-ville").closest("div").parent();
            div.addClass("has-error has-feedback");
            ret = false;
        }
        // else
        // {
            // $(".txt-ville-error").hide();
            // var div = $(".txt-ville").closest("div").parent();
            // div.removeClass("has-error");
            // ret = true;
        // }

        /*if(zRcs== '')
        {
            $(".txt-rcs-error").show();
            var div = $(".txt-rcs").closest("div").parent();
            div.addClass("has-error has-feedback");
            ret = false;
        }*/
		
		//http://bo.loc-courtoisie.fr/admin/customer/check-siren-exist/?siren=834568560&client=
		
		
        // else
        // {
            // $(".txt-rcs-error").hide();
            // var div = $(".txt-rcs").closest("div").parent();
            // div.removeClass("has-error");
            // ret = true;
        // }

        if(zNom== '')
        {
            $(".txt-nom-error").show();
            var div = $(".txt-nom").closest("div").parent();
            div.addClass("has-error has-feedback");
            ret = false;
        }
        // else
        // {
            // $(".txt-nom-error").hide();
            // var div = $(".txt-nom").closest("div").parent();
            // div.removeClass("has-error");
            // ret = true;
        // }

        if(zTelephone== '')
        {
            $(".txt-telephone-error").show();
            var div = $(".txt-telephone").closest("div").parent();
            div.addClass("has-error has-feedback");
            ret = false;
        }
        // else
        // {
            // $(".txt-telephone-error").hide();
            // var div = $(".txt-telephone").closest("div").parent();
            // div.removeClass("has-error");
            // ret = true;
        // }

        /*if(zFax== '')
        {
            $(".txt-fax-error").show();
            var div = $(".txt-fax").closest("div").parent();
            div.addClass("has-error has-feedback");
            ret = false;
        }*/
        // else
        // {
            // $(".txt-fax-error").hide();
            // var div = $(".txt-fax").closest("div").parent();
            // div.removeClass("has-error");
            // ret = true;
        // }

        if(zEmail== '' || !validateEmail(zEmail))
        {
            $(".txt-email-error").show();
            var div = $(".txt-email").closest("div").parent();
            div.addClass("has-error has-feedback");
            ret = false;
        }
        // else
        // {
            // $(".txt-email-error").hide();
            // var div = $(".txt-email").closest("div").parent();
            // div.removeClass("has-error");
            // ret = true;
        // }

        if(zAdresseLivr== '')
        {
            $(".txt-adresse-livraison-error").show();
            var div = $(".txt-adresse-livraison").closest("div").parent();
            div.addClass("has-error has-feedback");
            ret = false;
        }
        // else
        // {
            // $(".txt-adresse-livraison-error").hide();
            // var div = $(".txt-adresse-livraison").closest("div").parent();
            // div.removeClass("has-error");
            // ret = true;
        // }

        if(zCpLivr== '' || zCpLivr.length != 5)
        {
            $(".txt-cp-livraison-error").show();
            var div = $(".txt-cp-livraison").closest("div").parent();
            div.addClass("has-error has-feedback");
            ret = false;
        }
        // else
        // {
            // $(".txt-cp-livraison-error").hide();
            // var div = $(".txt-cp-livraison").closest("div").parent();
            // div.removeClass("has-error");
            // ret = true;
        // }

        if(zVilleLivr== '')
        {
            $(".txt-ville-livraison-error").show();
            var div = $(".txt-ville-livraison-livraison").closest("div").parent();
            div.addClass("has-error has-feedback");
            ret = false;
        }
        // else
        // {
            // $(".txt-ville-livraison-error").hide();
            // var div = $(".txt-ville-livraison").closest("div").parent();
            // div.removeClass("has-error");
            // ret = true;
        // }

        var urlParams = "";
		
		if(zSiren== '')
        {
            $(".txt-siren-error").show();
            var div = $(".txt-siren").closest("div").parent();
            div.addClass("has-error has-feedback");
            ret = false;
        }else{
			$.ajax({
				url: 'http://bo.loc-courtoisie.fr/admin/customer/check-siren-exist/?siren='+ zSiren +'&client=&from_other=1',
				dataType: "jsonp",
				jsonpCallback: 'apiStatus',
				type: 'GET'
			}).then(function(data, textStatus, jqXHR ){
				debugger;
				if ( !data && zRcs == '' && $('.hnew').val() == '' ){
					$(".txt-rcs-error").show();
					var div = $(".txt-rcs").closest("div").parent();
					div.addClass("has-error has-feedback");
					ret = false;
				}
				if ( ret ) {
					$(".has-error").removeClass("has-error");
					urlParams += "societe="+zSociete;
					urlParams += "&adresse="+zAdresse;
					urlParams += "&cp="+zCp;
					urlParams += "&ville="+zVille;
					urlParams += "&rcs="+zRcs;
					urlParams += "&siren="+zSiren;
					urlParams += "&nom="+zNom;
					urlParams += "&tel="+zTelephone;
					urlParams += "&fax="+zFax;
					urlParams += "&email="+zEmail;
					urlParams += "&adresse_livr="+zAdresseLivr;
					urlParams += "&cp_livr="+zCpLivr;
					urlParams += "&ville_livr="+zVilleLivr;
					urlParams += "&duree="+zDuree;
					urlParams += "&km="+zKm;
					urlParams += "&contrat="+zContrat;
					urlParams += "&couleur="+zCouleur;
					urlParams += "&quantite="+zQuantite;
					urlParams += "&prix="+zPrix;
					urlParams += "&civilite="+zCivilite;
					urlParams += "&version="+zVersion;
					urlParams += "&extension="+zExtension;
					urlParams += "&perte="+zPerte; 	
					urlParams += "&site_name="+zSitename;
					urlParams += "&id_site="+iIdSite;

					window.location.href = $('.valider_commande').attr('urlto') + "/" + urlParams;
				}
			});

			
		}

    });
	
	//Save contact
	$('.valider_contact').click(function(){
        $('.has-error').removeClass("has-error");
        $('.has-feedback').removeClass("has-feedback");
		var ret = true;
		var zContact_financement 	= $("#contact_financement").val();
		var zContact_name 			= $("#contact_name").val();
		var zContact_email 			= $("#contact_email").val();
		var zContact_telephone 		= $("#contact_telephone").val();
		var zContact_model 			= $("#contact_model").val();
		var zContact_message 		= $("#contact_message").val();
		var zContact_km_annuel 		= $("#km_annuel").val();
		var zSite_name 		        = $(".hsite_name").val();
		var zSite_id 		        = $(".hsite_id").val();


        if( zContact_name == '' )
        {
            $(".contact_name_error").show();
            var div = $("#contact_name").closest("div").parent();
            div.addClass("has-error has-feedback");
            ret = false;
        }
        else
        {
            $(".contact_name_error").hide();
            var div = $("#contact_name").closest("div").parent();
            div.removeClass("has-error");
		}
		
		if( zContact_email == '' || !validateEmail(zContact_email) )
        {
            $(".contact_email_error").show();
            var div = $("#contact_email").closest("div").parent();
            div.addClass("has-error has-feedback");
            ret = false;
        }
        else
        {
            $(".contact_email_error").hide();
            var div = $("#contact_email").closest("div").parent();
            div.removeClass("has-error");
		}
		
		if( zContact_message == '' )
        {
            $(".contact_message_error").show();
            var div = $("#contact_message").closest("div").parent();
            div.addClass("has-error has-feedback");
            ret = false;
        }
        else
        {
            $(".contact_message_error").hide();
            var div = $("#contact_message").closest("div").parent();
            div.removeClass("has-error");
		}
		
		var urlParams = "1=1";

        if ( ret ) {
            urlParams += "&financement="+zContact_financement; 	
            urlParams += "&name="+zContact_name; 			
            urlParams += "&email="+zContact_email;			
            urlParams += "&telephone="+zContact_telephone; 		
            urlParams += "&model="+zContact_model; 					
            urlParams += "&message="+zContact_message;		
            urlParams += "&km="+zContact_km_annuel;
            urlParams += "&site_name="+zSite_name;
            urlParams += "&site_id="+zSite_id;

            window.location.href = $(this).attr('urlto') + "/" + urlParams;
        }
	});
});