var initBreadCrumb = function(){
	
	var txtBreadCrumb = "";
	
	$('#searchbar-category').next().find('input:checkbox').each( function() {
		if ($(this).parent().hasClass('ui-checkboxradio-checked')  && $(this).parent().hasClass('searchbar_cat')) {
			if ( txtBreadCrumb == "" ) {
				txtBreadCrumb = $(this).attr("attr-label");
			}else{
				txtBreadCrumb = txtBreadCrumb + " - " + $(this).attr("attr-label");
			}
		}

		if ($(this).parent().hasClass('ui-checkboxradio-checked') && $(this).parent().hasClass('searchbar_type')) {
			if ( txtBreadCrumb == "" ) {
				txtBreadCrumb = $(this).attr("attr-label");
			}else{
				txtBreadCrumb = txtBreadCrumb + " - " + $(this).attr("attr-label");
			}
		}
	});
	
	$('.block-brand').find('input:checkbox').each( function() {
		if ($(this).parent().hasClass('ui-checkboxradio-checked')) {
			if ( txtBreadCrumb == "" ) {
				txtBreadCrumb = $(this).attr("attr-label");
			}else{
				txtBreadCrumb = txtBreadCrumb + " - " + $(this).attr("attr-label");
			}
		}
	});

	$('.block-portes').find('input:checkbox').each( function() {
		if ($(this).parent().hasClass('ui-checkboxradio-checked')) {
			if ( txtBreadCrumb == "" ) {
				txtBreadCrumb = $(this).val() + " portes";
			}else{
				txtBreadCrumb = txtBreadCrumb + " - " + $(this).val() + " portes";
			}
		}
	});
	
	$('.block-carburant').find('input:checkbox').each( function() {
		if ($(this).parent().hasClass('ui-checkboxradio-checked')) {
			if ( txtBreadCrumb == "" ) {
				txtBreadCrumb = $(this).attr("attr-label");
			}else{
				txtBreadCrumb = txtBreadCrumb + " - " + $(this).attr("attr-label");
			}
		}
	});
	
	if ( txtBreadCrumb != "" )
		$('.keywords').html(txtBreadCrumb);
	else
		$('.keywords').html('Aucun filtre selectionné');
}

$(document).ready(function(){
    var rangeCategory = "";
    var rangeType = "";
    var rangeBudget = "";
    var rangeKm = "";
    var rangeMarque = "";
    var rangeCarburant = "";
    var rangePorte = "";
    var rangeDuree = "";

    var urlParams = "";
	
	initBreadCrumb();

    $('.submit').click(function(){

        //rangeBudget = "[" + $("#range-budget .handle-left .value").text() + "," + $("#range-budget .handle-right .value").text() + "]";
        rangeKm =  $("#range-distance .handle-right .value").text() ;

        $('#searchbar-category').next().find('input:checkbox').each( function() {
            if ($(this).parent().hasClass('ui-checkboxradio-checked')  && $(this).parent().hasClass('searchbar_cat')) {
                if ( rangeCategory == "" ) {
                    rangeCategory = $(this).val();
                }else{
                    rangeCategory = rangeCategory + "," + $(this).val();
                }
            }

            if ($(this).parent().hasClass('ui-checkboxradio-checked') && $(this).parent().hasClass('searchbar_type')) {
                if ( rangeType == "" ) {
                    rangeType = $(this).val();
                }else{
                    rangeType = rangeType + "," + $(this).val();
                }
            }
        });


        $('.block-carburant').find('input:checkbox').each( function() {
            if ($(this).parent().hasClass('ui-checkboxradio-checked')) {
                if ( rangeCarburant == "" ) {
                    rangeCarburant = $(this).val();
                }else{
                    rangeCarburant = rangeCarburant + "," + $(this).val();
                }
            }


        });


        $('.block-brand').find('input:checkbox').each( function() {
            if ($(this).parent().hasClass('ui-checkboxradio-checked')) {
                if ( rangeMarque == "" ) {
                    rangeMarque = $(this).val();
                }else{
                    rangeMarque = rangeMarque + "," + $(this).val();
                }
            }
        });

        $('.block-portes').find('input:checkbox').each( function() {
            if ($(this).parent().hasClass('ui-checkboxradio-checked')) {
                if ( rangePorte == "" ) {
                    rangePorte = $(this).val();
                }else{
                    rangePorte = rangePorte + "," + $(this).val();
                }
            }
        });

        $('.block-duree').find('input:checkbox').each( function() {
            if ($(this).parent().hasClass('ui-checkboxradio-checked')) {
                if ( rangeDuree == "" ) {
                    rangeDuree = $(this).val();
                }else{
                    rangeDuree = rangeDuree + "," + $(this).val();
                }
            }
        });

        //if ( rangeBudget != "" )
        urlParams += "1=1";
        if ( rangeCategory != "" )  urlParams += "&category=" + rangeCategory;
        if ( rangeType != "" )      urlParams += "&sous_category=" + rangeType;
        if ( rangeKm != "" )        urlParams += "&km=" + rangeKm;

        if ( rangeMarque != "" )    urlParams += "&marque=" + rangeMarque + "";
        if ( rangeCarburant != "" ) urlParams += "&carburant=" + rangeCarburant + "";
        if ( rangePorte != "" )     urlParams += "&porte=" + rangePorte + "";
        if ( rangeDuree != "" )     urlParams += "&duree=" + rangeDuree + "";

        window.location.href = window.location.href = $(this).attr('urlto') + "/" + urlParams;

    });
	
	$(".tr_finition").click(function(){
		window.location.href = $(this).attr('urlto');
	});
});