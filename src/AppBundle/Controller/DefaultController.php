<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request as HttpRequest;
use Symfony\Component\HttpFoundation\RedirectResponse;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $client = new Client();
        $zListProduit = "";
        /*$request = new \GuzzleHttp\Psr7\Request('GET', 'http:/localhost/', [
            'Host' => 'mycoolsite.com'
        ]);
        $response = (new GuzzleHttp\Client())->send($request, ['debug' => true]);*/

        $res2 = $client->request('GET', 'http://127.0.0.1/'.$this->getParameter('full_url_provider').'/app_dev.php/apis/'.$this->getParameter('id_site').'/products/top3=true', [
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-type' => 'application/json',
                    'Host' => $this->getParameter('host_provider')
                ]]);
        $tProduit = \GuzzleHttp\json_decode($res2->getBody(true)->getContents());
		
		$res2 = $client->request('GET', 'http://127.0.0.1/'.$this->getParameter('full_url_provider').'/app_dev.php/apis/'.$this->getParameter('id_site').'/products/top6=true', [
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-type' => 'application/json',
                    'Host' => $this->getParameter('host_provider')
                ]]);
        $tProduitHeader = \GuzzleHttp\json_decode($res2->getBody(true)->getContents());

        foreach($tProduit as $oProduit) {
            $zListProduit .= $zListProduit == "" ? $oProduit->id_produit : "," . $oProduit->id_produit  ;
        }
        $tCaracteristique = array();
        if ( $zListProduit ) {
            $res = $client->request('GET', 'http://127.0.0.1/'.$this->getParameter('full_url_provider').'/app_dev.php/apiproducts/'.$zListProduit.'/caracteristique', [
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-type' => 'application/json',
                    'Host' => $this->getParameter('host_provider')
                ]]);
            $tRefCaracteristique = \GuzzleHttp\json_decode($res->getBody(true)->getContents());


            foreach ($tRefCaracteristique as $oCaracteristique){
                $tCaracteristique[$oCaracteristique->id_produit][] = $oCaracteristique;
            }
        }
		
		$resModule = $client->request('GET', 'http://127.0.0.1/'.$this->getParameter('full_url_provider').'/app_dev.php/api/modules', [
            'headers' => [
                'Accept' => 'application/json',
                'Content-type' => 'application/json',
                'Host' => $this->getParameter('host_provider')
            ]]);
        $tModule = \GuzzleHttp\json_decode($resModule->getBody(true)->getContents());

        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
            'produits' => $tProduit,
            'produits_header' => $tProduitHeader,
            'caracteristiques' => $tCaracteristique,
			'modules' => $tModule
        ));
    }

    /**
     * @Route("/product/list", name="search")
     */
    public function listAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/list.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
            'produits' => ''
        ));
    }

	/**
     * @Route("/save-contact/{params}", name="save-contact")
     */
    public function saveContact ( $params ){
        $client = new Client();
        $zListProduit = "";
        $res = $client->request('GET', 'http://127.0.0.1/'.$this->getParameter('full_url_provider').'/app_dev.php/apisaves/'.urldecode($params).'/contact', [
            'headers' => [
                'Accept' => 'application/json',
                'Content-type' => 'application/json',
                'Host' => $this->getParameter('host_provider')

            ]]);
        $tRet = \GuzzleHttp\json_decode($res->getBody(true)->getContents());

        if ( $tRet ) {
			$this->get('session')->getFlashBag()->set('success', 'Message envoyé');

            return $this->redirectToRoute('contact');
        }
		
    }

    /**
     * @Route("/save-command/{params}", name="save-command")
     */
    public function saveCommande ( $params = null ){
        $client = new Client();
        $zListProduit = "";
        $res = $client->request('GET', 'http://127.0.0.1/'.$this->getParameter('full_url_provider').'/app_dev.php/apisaves/'.urldecode($params).'/commande' , [
            'headers' => [
                'Accept' => 'application/json',
                'Content-type' => 'application/json',
                'Host' => $this->getParameter('host_provider')

            ]]);
        //var_dump($res->getBody(true)->getContents());exit;
        $tRet = \GuzzleHttp\json_decode($res->getBody(true)->getContents());

		$this->get('session')->getFlashBag()->set('success', 'Devis envoyé');
		$referer = $this->get('request')->headers->get('referer');   
		return new RedirectResponse($referer);

    }

    /**
     * @Route("/comment-ca-marche", name="comment-ca-marche")
     */
    public function commentCaMarcheAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/comment-ca-marche.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR
        ));
    }

    /**
     * @Route("/obligations", name="obligations")
     */
    public function obligationsAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/vos-obligations.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR
        ));
    }

    /**
     * @Route("/restitution", name="restitution")
     */
    public function restitutionAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/la-restitution.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR
        ));
    }
    /**
     * @Route("/assurance-financiere", name="assurance-financiere")
     */
    public function assuranceFinanciereAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/assurance-financiere.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR
        ));
    }
	
	/**
     * @Route("/mentions-legales", name="mentions-legales")
     */
    public function mentionsLegalesAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/mentions-legales.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
			'site_name' => $this->getParameter('site_name')
        ));
    }

    /**
     * @Route("/no-product", name="no-product")
     */
    public function noProductAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/no-product.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR
        ));
    }

    /**
     * @Route("/votre-commande", name="votre-commande")
     */
    public function votreCommandeAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/votre-commande.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR
        ));
    }

    /**
     * @Route("/product/detail/{id_product}/{id_extension}", name="product-detail")
     */
    public function detailProductAction(Request $request, $id_product, $id_extension)
    {
        $iIdProduct = $id_product;
        $client = new Client();
        $res = $client->request('GET', 'http://127.0.0.1/'.$this->getParameter('full_url_provider').'/app_dev.php/apis/'.$this->getParameter('id_site').'/products/id_product='. $iIdProduct, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-type' => 'application/json',
                'Host' => $this->getParameter('host_provider')

            ]]);
        $tProduit = \GuzzleHttp\json_decode($res->getBody(true)->getContents());

        
        $tOption = array();

        $res = $client->request('GET', 'http://127.0.0.1/'.$this->getParameter('full_url_provider').'/app_dev.php/apiproducts/'.$this->getParameter('id_site').'/optionbyrefs/'.$id_extension, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-type' => 'application/json',
                'Host' => $this->getParameter('host_provider')
            ]]);
        $tRefOption = \GuzzleHttp\json_decode($res->getBody(true)->getContents());		
		
		$resModule = $client->request('GET', 'http://127.0.0.1/'.$this->getParameter('full_url_provider').'/app_dev.php/api/modules', [
            'headers' => [
                'Accept' => 'application/json',
                'Content-type' => 'application/json',
                'Host' => $this->getParameter('host_provider')
            ]]);
        $tModule = \GuzzleHttp\json_decode($resModule->getBody(true)->getContents());

        return $this->render('default/detail-product.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
            'produits' => $tProduit,
			'options'  => $tRefOption,
			'modules' => $tModule
        ));
    }

    /**
     * @Route("/product/detail-intermediaire/{id_product}", name="product-detail-intermediaire")
     */
    public function detailProductIntermediaireAction(Request $request, $id_product)
    {
        $iIdProduct = $id_product;
        $client = new Client();
        $res = $client->request('GET', 'http://127.0.0.1/'.$this->getParameter('full_url_provider').'/app_dev.php/apis/'.$this->getParameter('id_site').'/products/id_product='. $iIdProduct, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-type' => 'application/json',
                'Host' => $this->getParameter('host_provider')

            ]]);
        $tProduit = \GuzzleHttp\json_decode($res->getBody(true)->getContents());

        $resVersion = $client->request('GET', 'http://127.0.0.1/'.$this->getParameter('full_url_provider').'/app_dev.php/apiproducts/'.$this->getParameter('id_site').'/versions/'.$iIdProduct, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-type' => 'application/json',
                'Host' => $this->getParameter('host_provider')

            ]]);
        $tVersion = \GuzzleHttp\json_decode($resVersion->getBody(true)->getContents());

        $tOption = array();

        $res = $client->request('GET', 'http://127.0.0.1/'.$this->getParameter('full_url_provider').'/app_dev.php/apiproducts/'.$iIdProduct.'/option', [
            'headers' => [
                'Accept' => 'application/json',
                'Content-type' => 'application/json',
                'Host' => $this->getParameter('host_provider')
            ]]);
        $tRefOption = \GuzzleHttp\json_decode($res->getBody(true)->getContents());


        foreach ($tRefOption as $oOption){
            $tOption[] = $oOption;
        }

        $tKm        = array();
        $tDuree     = array();
        $tContrat   = array();

        foreach( $tOption as $lgOption ){
            if ( $lgOption->is_actif == 1 ) {
                if ( $lgOption->id_produit_attribut == 2 ){
                    $tDuree[$lgOption->id] = $lgOption->nom;
                }

                if ( $lgOption->id_produit_attribut == 1 ){
                    $tKm[$lgOption->id] = $lgOption->nom;
                }

                if ( $lgOption->id_produit_attribut == 4 ){
                    $tContrat[$lgOption->id] = $lgOption->nom;
                }
            }
        }

		$resModule = $client->request('GET', 'http://127.0.0.1/'.$this->getParameter('full_url_provider').'/app_dev.php/api/modules', [
            'headers' => [
                'Accept' => 'application/json',
                'Content-type' => 'application/json',
                'Host' => $this->getParameter('host_provider')
            ]]);
        $tModule = \GuzzleHttp\json_decode($resModule->getBody(true)->getContents());

		$tParamsSameCategory = 'id_produit='.$tProduit[0]->id_produit.'&prix='.intval($tProduit[0]->prix_modif);
		
		$resSameCategory = $client->request('GET', 'http://127.0.0.1/'.$this->getParameter('full_url_provider').'/app_dev.php/apiproducts/'.$this->getParameter('id_site').'/samecategories/'. $tParamsSameCategory, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-type' => 'application/json',
                'Host' => $this->getParameter('host_provider')

            ]]);
        $tProduitSameCategory = \GuzzleHttp\json_decode($resSameCategory->getBody(true)->getContents());

        return $this->render('default/detail-product-intermediaire.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
            'produits' => $tProduit,
            'options_km' => $tKm,
            'options_duree' => $tDuree,
            'options_contrat' => $tContrat,
            'versions' => $tVersion,
			'modules' => $tModule,
			'samecategories' => $tProduitSameCategory
        ));
    }

    /**
     * @Route("/produits-complementaires", name="produits-complementaires")
     */
    public function produitsComplementairesAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/produits-complementaires.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR
        ));
    }

    /**
     * @Route("/contact", name="contact")
     */
    public function contactAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/contact.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR
        ));
    }

    /**
     * @Route("/infos-pratiques", name="infos-pratiques")
     */
    public function infosPratiquesAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/infos-pratiques.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR
        ));
    }

    /**
    * @Route("/product/find/{params}", name="search_result")
    */
    public function guzzlewrapperAction($params = null)
    {
        $client = new Client();
        $zListProduit = "";
        $tRequest = array();
		
        $res = $client->request('GET', 'http://127.0.0.1/'.$this->getParameter('full_url_provider').'/app_dev.php/apis/'.$this->getParameter('id_site').'/products/'. urldecode($params), [
            'headers' => [
                'Accept' => 'application/json',
                'Content-type' => 'application/json',
                'Host' => $this->getParameter('host_provider')

            ]]);
        $tProduit = \GuzzleHttp\json_decode($res->getBody(true)->getContents());

		$resModule = $client->request('GET', 'http://127.0.0.1/'.$this->getParameter('full_url_provider').'/app_dev.php/api/modules', [
            'headers' => [
                'Accept' => 'application/json',
                'Content-type' => 'application/json',
                'Host' => $this->getParameter('host_provider')
            ]]);
        $tModule = \GuzzleHttp\json_decode($resModule->getBody(true)->getContents());
		
        $params = explode('&', $params);

        if ( count($params) > 1) {
            foreach ($params as $lgParams) {
                $lgRequest = explode('=', $lgParams);
                $tRequest[$lgRequest[0]] = str_replace(']', '', str_replace('[', '', $lgRequest[1]));
            }
        }

		$tmpFiltre = array();
		
		$tmpFiltre['porte']  = array();
		$tmpFiltre['km1'] = 0;
		$tmpFiltre['km2'] = 0;
		$tmpFiltre['duree'] = array();
		$tmpFiltre['carburant'] = array();
		
		if ( array_key_exists('category', $tRequest) )
            $tmpFiltre['category'] = explode(',', $tRequest['category']);

        if ( array_key_exists('sous_category', $tRequest) )
            $tmpFiltre['sous_category'] = explode(',', $tRequest['sous_category']);

        if ( array_key_exists('marque', $tRequest) )
            $tmpFiltre['marque'] = explode(',', $tRequest['marque']);


        if ( array_key_exists('porte', $tRequest) )
            $tmpFiltre['porte'] = explode(',', $tRequest['porte']);

        if ( array_key_exists('km', $tRequest) ){
            $tmpKmTotal = explode(',', $tRequest['km']);

            if ( count($tmpKmTotal) == 1 )
			{
				$tmpFiltre['km1'] = $tmpKmTotal[0];
                $tmpFiltre['km2'] = $tmpKmTotal[0];
			}                
            else
			{
				$tmpFiltre['km1'] = $tmpKmTotal[0];
                $tmpFiltre['km2'] = $tmpKmTotal[1];
			}
                
        }

        if ( array_key_exists('duree', $tRequest) ){
            $tmpFiltre['duree'] = explode(',', $tRequest['duree']);
        }

		if ( array_key_exists('carburant', $tRequest) )
            $tmpFiltre['carburant'] = explode(',', $tRequest['carburant']);

        if ( count($tProduit) == 0 ) {
            return $this->render('default/no-product.html.twig', array(
                'modules' => $tModule,
                'filtres' => $tmpFiltre));
        }

        return $this->render('default/list.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
            'produits' => $tProduit,
            'modules' => $tModule,
			'filtres' => $tmpFiltre
        ));
    }
}
