'use strict';

$(document).ready(function () {
	function detectIE() {
		var ua = window.navigator.userAgent;

		var msie = ua.indexOf('MSIE ');
		if (msie > 0) {
			// IE 10 or older => return version number
			return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
		}

		var trident = ua.indexOf('Trident/');
		if (trident > 0) {
			// IE 11 => return version number
			var rv = ua.indexOf('rv:');
			return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
		}

		var edge = ua.indexOf('Edge/');
		if (edge > 0) {
		   // Edge (IE 12+) => return version number
		   return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
		}

		// other browser
		return false;
	}
	if ( detectIE() != false && !isNaN(detectIE()) && detectIE() < 10 )
	{
		alert('Vous utilisez une ancienne version de Internet Explorer (IE '+detectIE()+'), veuillez le mettre à jour ou veuillez utiliser google chrome ou Mozilla Firefox pour une meilleur expérience');
	}

  ui.links.dummyLink(); //avoid page scrolling when an empty link is clicked
  ui.links.scrollTo(); //give a link the ability of scrolling to an anchor

  ui.elements.searchbar.dropdown(); //custom dropdown for searchbar
  ui.elements.featured.carsDetails(); //display more information for car blocs
  ui.elements.search.rangeSlider(); //custom dropdown for searchbar
  ui.elements.result.toggle(); //show / hide search results
  ui.elements.searchEngine.toggle(); //show / hide search results
  ui.elements.header.affix(); //header affix
  ui.elements.footer.affix(); //header affix

  ui.form.selectmenu(); //customize select elements
  ui.form.checkbox(); //customize checkbox elements
  ui.form.radio(); //customize radio elements
  ui.form.textarea(); //autosize textarea elements

  ui.progression.init(); //pagination accordion

  ui.sliders.banner();

  ui.objects.matchHeight(); //give the same height to targeted objects
  page.details.duplicate_table(); //make a responsive version of the table
});
var page = {
  details: {
    duplicate_table: function duplicate_table() {
      var $table = $('[id*=promoTable] .table-responsive table#result');
      var $table_container = $('[id*=promoTable] .table-responsive');
      $table_container.addClass('hidden-xs');

      if ($table.length) {
        var $table_header = [];
        $table.find('thead>tr>td>strong').each(function () {
          $table_header.push($(this).text());
        });

        var $block = $('<div id="result_block" class="visible-xs"></div>');
        $table_container.after($block);

        $table.find('tbody>tr').each(function () {
          var $list = $('<dl></dl>');
          var $tr = $(this).clone();
          var $tr_url = $tr.attr('urlto') || '';
          var $td = $tr.find('td');

          $td.each(function () {
            var $this = $(this);
            $list.append($('<dt><a href="' + $tr_url + '" title="' + $this.text() + '">' + $table_header[$this.index()] + '</a></dt><dd><a href="' + $tr_url + '">' + $this.text() + '</a></dd>'));
          });

          $block.append($list);
        });
      }
    }
  }
};
var ui = {
  progression: {
    init: function init() {
      var $link = $('#progression li a, #content-tabs .col a.img-container');
      var $content = $('#progression~.content-body, #content-tabs ~ .content-body');
      if (eval($link.length + $content.length) >= 2) {
        $link.on('click', function () {
          var $this = $(this);
          var index = $(this).closest('li, .col').index();
          $link.removeClass('active');
          $this.addClass('active');
          $content.hide();
          $content.eq(index).show();
        });
      }
    }
  },
  links: {
    dummyLink: function dummyLink() {

      /*-- avoid page scrolling when an empty link is clicked --*/
      $('a[href="#"]').on('click', function (e) {
        e.preventDefault();
      });
      /*-- end: avoid page scrolling when an empty link is clicked --*/
    },
    scrollTo: function scrollTo() {

      /*-- give a link the ability of scrolling to an anchor --*/
      var $link = $('a.action-scrollTo');
      var $body = $('html, body');
      if ($link.length) {

        $link.on('click', function (e) {
          e.preventDefault();
          var $this = $(this);
          var target = $($this.attr('href')).offset().top;
          $body.animate({ 'scrollTop': target }, 750);
        });
        /*-- give a link the ability of scrolling to an anchor --*/
      }
    }
  },
  sliders: {
    banner: function banner() 
	{
      var $slider = $('#banner #slider');
      if ($slider.length) {
        $slider.slick({
          slidesToShow: 3,
          slidesToScroll: 1,
		  autoplay: true,
		  autoplaySpeed: 1000,
          prevArrow: '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button"><i class="icon icon-arrow-left"></i></button>',
          nextArrow: '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button"><i class="icon icon-arrow-right"></i></button>',
          responsive: [{
            breakpoint: 1024,
            settings: {
              slidesToShow: 2,
			  slidesToScroll: 1,
			  autoplay: true,
		      autoplaySpeed: 1000
            }
          }, {
            breakpoint: 768,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
			  autoplay: true,
		      autoplaySpeed: 1000
            }
          }]
        });
      }
	  $("#slider").show();
    }
  },
  objects: {
    matchHeight: function matchHeight() {

      /*- give the same height to targeted elements --*/
      var $matchHeight = $('.matchHeight');
      if ($matchHeight.length) {
        $matchHeight.matchHeight();
      }

      var $match1 = $('.match-1');
      if ($match1.length) {
        $match1.matchHeight();
      }

      var $match2 = $('.match-2');
      if ($match2.length) {
        $match2.matchHeight();
      }

      var $match3 = $('.match-3');
      if ($match3.length) {
        $match3.matchHeight();
      }
      /*- end: give the same height to targeted elements --*/
    }
  },
  form: {
    selectmenu: function selectmenu() {
      var $select = $('select.select');
      if ($select.length) {
        $select.each(function () {
          var $this = $(this);

          //import data-icon attribute value as icon class
          if (typeof $this.attr('data-icon') !== 'undefined') {
            var $icon = 'icon icon-' + $this.attr('data-icon');
          } else {
            var $icon = 'ui-icon-triangle-1-s';
          }

          $this.selectmenu({
            classes: {
              'ui-selectmenu-button': 'ui-selectmenu-button form-control'
            },
            icons: {
              button: $icon
            }
          });
        });
      }
    },
    checkbox: function checkbox() {
      var $checkbox = $('input[type="checkbox"].checkbox');
      if ($checkbox.length) {
        $checkbox.checkboxradio({
          classes: {
            'ui-checkboxradio-icon': 'icon icon-check'
          }
        });
      }
    },
    radio: function radio() {
      var $radio = $('input[type="radio"].radio');
      if ($radio.length) {
        $radio.checkboxradio({
          classes: {
            'ui-checkboxradio-icon': 'icon icon-check'
          }
        });
      }
    },
    textarea: function textarea() {
      var $textarea = $('textarea');
      if ($textarea.length) {
        autosize($textarea);
      }
    }
  },
  elements: {
    header: {
      affix: function affix() {
        var $window = $(window);
        var $header = $('#header.section .section-header');
        if ($header.length) {
          var $page = $('#page');
          $window.on('scroll', function () {
            if ($window.scrollTop() > 0) {
              $page.addClass('pin');
            } else {
              $page.removeClass('pin');
            }
          });
        }
      }
    },
    footer: {
      affix: function affix() {
        var $page = $('#page');
        var $window = $(window);
        var $footer = $('#footer');
        var $header = $('#header .section-header');

        var update_footer = function method() {
          $page.css('padding-bottom', $footer.outerHeight());
          $page.css('padding-top', $header.outerHeight());
          return method;
        }();
        $window.on('resize', update_footer);
        //$window.on('load', update_footer);
      }
    },
    search: {
            rangeSlider: function rangeSlider() {
                var $ranges = $('#range-budget, #range-distance');

                if ($($ranges.length)) {
                    $('#range-budget').slider({
                        range: true,
                        min: 69,
                        max: 600,
                        values: [69, 600],
                        slide: function slide(event, ui) {
                            $range_budget_l.text(ui.values[0]);
                            $range_budget_r.text(ui.values[1]);
                        }
                    });

					if ( $('.init_r_km').val() != '0' && $('.init_r_km').val() != ''  )
					{
						$('#range-distance').slider({
							min: 10000,
							max: 60000,
							step: 5000,
							values: [$('.init_r_km').val()],
							slide: function slide(event, ui) {
								$range_distance_r.text(ui.values[0]);
							}
						});
						var $range_distance_r = $('#range-distance .handle-right .value');
						
						$range_distance_r.text($('#range-distance').slider('values', 0));
					}else{
						$('#range-distance').slider({
							min: 10000,
							max: 60000,
							step: 5000,
							values: [10000],
							slide: function slide(event, ui) {
								$range_distance_r.text(ui.values[0]);
							}
						});
						

						var $range_distance_r = $('#range-distance .handle-right .value');

						$range_distance_r.text($('#range-distance').slider('values', 0));
					}
                    

                    var $range_budget_l = $('#range-budget .handle-left .value');
                    var $range_budget_r = $('#range-budget .handle-right .value');

                    $range_budget_l.text($('#range-budget').slider('values', 0));
                    $range_budget_r.text($('#range-budget').slider('values', 1));

                    
                }
            }
        },
    searchEngine: {
      toggle: function toggle() {
        var $searchEngine = $('#searchEngine');
        var $btn = $('#searchEngine .inner #search .block-start.block .block-body .btn');
        var $promoTable = $('#promoTable');
        var $blocks = $('.block-middle, .block-fuel, .block-end');
        if ($btn.length + $promoTable.length >= 2) {

          $btn.on('click', function () {
            if ($searchEngine.hasClass('active')) {
              $promoTable.slideUp();
            }
            $searchEngine.toggleClass('active');
            $blocks.toggle();
          });
        }
      }
    },
    result: {
      toggle: function toggle() {
        var $toggle = $('#result .result-header .btn');
        if ($toggle.length) {
          var $search = $('#result #search');
          var $result = $('#result');
          $toggle.on('click', function () {
            var $this = $(this);
            $search.slideToggle(function () {
              $result.toggleClass('active');
              $this.toggleClass('active');
            });
          });
        }
      }
    },
    searchbar: {
      dropdown: function dropdown() {
        /*var $dropdown = $('[id*="search"].search-form .form-group .dropdown .form-control');*/
        /*var $dropdown_menu = $('[id*="search"].search-form .form-group .dropdown .form-control + .dropdown-menu');*/
        var $dropdown = $('.form-group .dropdown .form-control');
        var $dropdown_menu = $('.form-group .dropdown .form-control + .dropdown-menu');
        if ($dropdown.length) {
          $dropdown.on('click', function (e) {
            e.stopPropagation();
            var $this = $(this);
            var $target = $this.next('.dropdown-menu');
            $dropdown_menu.removeClass('open');
            $target.toggleClass('open');
          });
          $dropdown_menu.on('click', function (e) {
            e.stopPropagation();
          });
          $(document).on('click', function () {
            $dropdown_menu.removeClass('open');
          });
        }
      }
    },
    featured: {
      carsDetails: function carsDetails() {
        var $bloc_car = $('#featured .featured-body .block-car');
        var $button = $('#featured .featured-body .block-car a');
        if ($bloc_car.length) {
          $button.on('click', function () {
            var $this = $(this);
            var $parent_block = $(this).closest('.block-car');
            $bloc_car.removeClass('active hover');
            $parent_block.addClass('active');
          }).on('mouseenter', function () {
            var $this = $(this);
            var $parent_block = $(this).closest('.block-car');
            $bloc_car.removeClass('hover');
            $parent_block.addClass('hover');
          }).on('mouseleave', function () {
            var $this = $(this);
            var $parent_block = $(this).closest('.block-car');
            $bloc_car.removeClass('hover');
          });
        }

        $bloc_car.on('click', function (e) {
          e.stopPropagation();
        });

        $(document).on('click', function () {
          $bloc_car.removeClass('active hover');
        });
      }
    }
  }
};
//# sourceMappingURL=main.js.map
