<?php
/*
$app->get('/order/(:type)', function ($type = 'order') use ($app) {

	recherche_admin(__FILE__);

	$page = 1;
	if(isset($_SESSION['filtreAdmin'][__FILE__]['p']) AND is_numeric($_SESSION['filtreAdmin'][__FILE__]['p'])){
		$page = de_gpc_public($_SESSION['filtreAdmin'][__FILE__]['p']);
	}

	$limite = 20;
	if(isset($_SESSION['filtreAdmin'][__FILE__]['limite']) AND is_numeric($_SESSION['filtreAdmin'][__FILE__]['limite'])){
		$limite = de_gpc_public($_SESSION['filtreAdmin'][__FILE__]['limite']);
	}

	$db = Mysql::getInstance();

	$req_sites = $db->sqlQuery("SELECT * FROM sites");
	$sites =array();

	if($db->sqlNumRows($req_sites)){
		while($info = $db->sqlAssoc($req_sites)){
			$sites[$info['id']] = $info;
		}
	}

	Loader::loadClass('Commande_Etat_Liste_ListeEtat',PATH_CLASSES);

	$liste_etats = new Commande_Etat_Liste_ListeEtat();
	$liste_etats->addOrdre('ce.position','ASC');
	$liste_etats->getListeInfos();

	$form = new Form();

	$id_commande = $form -> createElement('id_commande');

	$date_debut = $form -> createElement('date_debut');
		$date_debut -> addValidator('Date');

	$date_fin = $form -> createElement('date_fin');
		$date_fin -> addValidator('Date');

	$total_debut = $form -> createElement('total_debut');
		$total_debut -> addValidator('NumFloat');

	$total_fin = $form -> createElement('total_fin');
		$total_fin -> addValidator('NumFloat');

	$etat = $form -> createElement('etat');

	$id_site = $form -> createElement('id_site');

	$client = $form -> createElement('client');

	$immatriculation = $form -> createElement('immatriculation');

	$modele = $form -> createElement('modele');

	$id_concessionnaire = $form -> createElement('id_concessionnaire');

	$id_financeur = $form -> createElement('id_financeur');

	$date_prevue_debut = $form -> createElement('date_prevue_debut');
		$date_prevue_debut -> addValidator('Date');

	$date_prevue_fin = $form -> createElement('date_prevue_fin');
		$date_prevue_fin -> addValidator('Date');
		$date_prevue_fin->setValueDefaut(date('d/m/Y',time()));

	$form->addElements(array($id_commande,$date_debut,$date_prevue_debut,$date_prevue_fin,$id_concessionnaire,$id_financeur,$date_fin,$total_debut,$total_fin,$etat,$id_site,$client,$immatriculation,$modele));

	Loader::loadClass('Commande_DetailCommande_Liste_ListeDetailCommande',PATH_CLASSES);
	$liste_cmd = new Commande_DetailCommande_Liste_ListeDetailCommande(true);
	$liste_cmd->setPageCourante($page);
	$liste_cmd->setNbResultatsPage($limite);
	$liste_cmd->addFiltre('valide','1');
	if($type != 'contrat')
		$liste_cmd->addFiltre('cit.is_delivred',0);
	else
		$liste_cmd->addFiltre('cit.is_delivred',1);
		$liste_cmd->addOrdre('c.date','DESC');
	if(count($_SESSION['filtreAdmin'][__FILE__]) AND $form->isValid($_SESSION['filtreAdmin'][__FILE__])){

		if(isset($_SESSION['filtreAdmin'][__FILE__]['id_commande']) AND !empty($_SESSION['filtreAdmin'][__FILE__]['id_commande']))
			$liste_cmd->addFiltre('c.id',$id_commande->getValue(),'=');
		if(!empty($_SESSION['filtreAdmin'][__FILE__]['date_debut']) AND !empty($_SESSION['filtreAdmin'][__FILE__]['date_fin']))
			$liste_cmd->addFiltre('c.date',array(dateToTimestamp($date_debut->getValue()),dateToTimestamp($date_fin->getValue())),'between');
		if(isset($_SESSION['filtreAdmin'][__FILE__]['total_debut']) AND !empty($_SESSION['filtreAdmin'][__FILE__]['total_debut']))
			$liste_cmd->addFiltre('c.total_ttc',$total_debut->getValue(),'>=');
		if(isset($_SESSION['filtreAdmin'][__FILE__]['total_fin']) AND !empty($_SESSION['filtreAdmin'][__FILE__]['total_fin']))
			$liste_cmd->addFiltre('c.total_ttc',$total_fin->getValue(),'<=');
		if(isset($_SESSION['filtreAdmin'][__FILE__]['etat']) AND $_SESSION['filtreAdmin'][__FILE__]['etat'] != 'all')
			$liste_cmd->addFiltre('cee.id_commande_etat',$etat->getValue());
		if(isset($_SESSION['filtreAdmin'][__FILE__]['id_site']) AND $_SESSION['filtreAdmin'][__FILE__]['id_site'] != 'all')
			$liste_cmd->addFiltre('c.id_site',$id_site->getValue());
		if(isset($_SESSION['filtreAdmin'][__FILE__]['client']) AND $_SESSION['filtreAdmin'][__FILE__]['client'] != '')
			$liste_cmd->addFiltre('cl.societe','%'.$client->getValue().'%','LIKE');
		if(isset($_SESSION['filtreAdmin'][__FILE__]['immatriculation']) AND $_SESSION['filtreAdmin'][__FILE__]['immatriculation'] != '')
			$liste_cmd->addFiltre('cit.immatriculation','%'.$immatriculation->getValue().'%','LIKE');
		if(!empty($_SESSION['filtreAdmin'][__FILE__]['id_concessionnaire']) AND $_SESSION['filtreAdmin'][__FILE__]['id_concessionnaire'] != 'all')
			$liste_cmd->addFiltre('cit.id_concessionnaire',$id_concessionnaire->getValue());
		if(!empty($_SESSION['filtreAdmin'][__FILE__]['id_financeur']) AND $_SESSION['filtreAdmin'][__FILE__]['id_financeur'] != 'all')
			$liste_cmd->addFiltre('cit.id_financeur',$id_financeur->getValue());
		if(!empty($_SESSION['filtreAdmin'][__FILE__]['date_prevue_debut']) AND !empty($_SESSION['filtreAdmin'][__FILE__]['date_prevue_fin']))
			$liste_cmd->addFiltre('cit.date_livraison_prev',array(dateToTimestamp($date_prevue_debut->getValue()),dateToTimestamp($date_prevue_fin->getValue())),'between');
		if(isset($_SESSION['filtreAdmin'][__FILE__]['modele']) AND $_SESSION['filtreAdmin'][__FILE__]['modele'] != '')
			$liste_cmd->addFiltre('cit.nom','%'.$modele->getValue().'%','LIKE');
	}

	if( in_array($type,array('order','contrat')))
	{
		$liste_cmd->addFiltre('cee.id_commande_etat',2);
	}
	else
	{
		$liste_cmd->addFiltre('cee.id_commande_etat',1);
	}

	$liste_cmd->setLimite($liste_cmd->getResultatCourant(),$liste_cmd->getNbResultatsPage());
	$liste_cmd->getListeInfos();
	$liste_cmd->getNbResultats();
	$concessionnaires = new Catalogue_Liste_ListeConcessionnaire(true);
	$concessionnaires->addOrdre('c.societe','ASC');
	$concessionnaires->getListeInfos();
	$concessionnaires->getListe();

	$financeurs = new Catalogue_Liste_ListeFinanceur(true);
	$financeurs->addOrdre('f.societe','ASC');
	$financeurs->getListeInfos();
	$financeurs->getListe();

	$datas = array();
	$datas['form'] = $form;
	$datas['list'] = $liste_cmd;
	$datas['page'] = $page;
	$datas['liste_sites'] = $sites;
	$datas['liste_etats'] = $liste_etats;
	$datas['concessionnaires'] = $concessionnaires;
	$datas['financeurs'] = $financeurs;
	$datas['title'] = (in_array($type,array('order','contrat'))) ? 'Liste des commandes' : 'Liste des devis';
	switch ($type) {
		case 'order':
			$datas['url'] = $app->urlFor('orders',array('type'=>'order'));
			break;
		case 'contrat':
			$datas['url'] = $app->urlFor('orders',array('type'=>'contrat'));
			break;

		default:
			$datas['url'] = $app->urlFor('orders',array('type'=>'estimate'));
			break;
	}

	$app->render('order'.DIRECTORY_SEPARATOR.'list.php',$datas);

})->name('orders');*/


/* ======================================================== */
/* ==================== NEW BO : ADMIN Cdes =============== */
/* ======================================================== */
$app->get('/order/(:type)', function ($type = 'order') use ($app) {

    recherche_admin(__FILE__);

    //MODELS
    $mdl_cdes = Model::Load('Commande_ModelExtends_Commande');
    $mdl_reseaux = Model::Load('Catalogue_ModelExtends_Reseau');
    $mdl_financeurs = Model::Load('Catalogue_ModelExtends_Financeur');
    $mdl_dealers = Model::Load('Catalogue_ModelExtends_Dealer');

    //LISTES COMMUNES
    $liste_financeurs = $mdl_financeurs->getFinanceursInfo(array());
    $liste_dealers = $mdl_dealers->getDealerInfo(array());


    /*
	 * VALIDATION FORM
	*/

    /*
	$liste_reseaux = $mdl_reseaux->find(array('limit'=>4));
	$reseaux = '';
	foreach ($liste_reseaux as $k=>$v){
		$reseaux .= sprintf('<br class="mb-5"/><div class="label label-default w-auto mb-5">%1$s</div>', $v['nom']);
	}
	*/


    /* gestion filtres forcés (devis/cde etc.) */
    //$cdes_conditions = array('fields'=>'c.*, c.id AS id_cde, cli.societe','order'=>'c.date DESC, c.id DESC','limit'=>20);
    $cdes_conditions = array('fields' => 'c.*, c.id AS id_cde, cli.societe', 'order' => 'c.date DESC, c.id DESC', 'limit' => 20);

    $_SESSION['filtreAdmin'][__FILE__]['valide'] = 1;

    if ($type == 'order') {

        $_SESSION['filtreAdmin'][__FILE__]['id_commande_etat'] = 2;
        $_SESSION['filtreAdmin'][__FILE__]['is_delivred'] = 0;

    } elseif ($type == 'estimate') {

        $_SESSION['filtreAdmin'][__FILE__]['id_commande_etat'] = 1;

    } elseif ($type == 'contrat') {

        $_SESSION['filtreAdmin'][__FILE__]['id_commande_etat'] = 2;
        $_SESSION['filtreAdmin'][__FILE__]['is_delivred'] = 1;

    }


    if(isset($_GET['action'])){
    	Loader::loadClass('Commande_Commande');
    	$commande = new Commande_Commande($_GET['id_commande']);
    	$id_new_devis = $commande->duplicate();
    	$app->redirect($app->urlFor('order_edit', array('id'=>$id_new_devis)));
    	exit;
    }
    /*
	if($type!='contrat') {
		$_SESSION['filtreAdmin'][__FILE__]['is_delivred']='';//0;
	} else {
		$_SESSION['filtreAdmin'][__FILE__]['is_delivred']=1;
	}

	if(in_array($type,array('order','contrat')))
	{
		$_SESSION['filtreAdmin'][__FILE__]['id_commande_etat']=2;
	} else {
		$_SESSION['filtreAdmin'][__FILE__]['id_commande_etat']='';//1;
	}*/


    /* gestion filtres forcés supplémentaires (devs) */
    /*
	$_SESSION['filtreAdmin'][__FILE__]['date_fin'] = '31/12/2016';
	$_SESSION['filtreAdmin'][__FILE__]['total_fin'] = 90;
	$_SESSION['filtreAdmin'][__FILE__]['valide'] = 1;
	*/

    /* gestion filtres dates timestamp */
    //si on a posté une date de début
    if (isset($_SESSION['filtreAdmin'][__FILE__]['date_debut']) & !empty($_SESSION['filtreAdmin'][__FILE__]['date_debut'])) {
        $_SESSION['filtreAdmin'][__FILE__]['date_debut'] = dateToTime($_SESSION['filtreAdmin'][__FILE__]['date_debut']);
    }

    //si on a posté une date de fin
    if (isset($_SESSION['filtreAdmin'][__FILE__]['date_fin']) & !empty($_SESSION['filtreAdmin'][__FILE__]['date_fin'])) {
        $_SESSION['filtreAdmin'][__FILE__]['date_fin'] = dateToTime($_SESSION['filtreAdmin'][__FILE__]['date_fin']);
    }

    $req_build = Model::Load('ModelRequestBuilder');
    $cdes_conditions['conditions'] = $req_build->compile(
        $req_build->setFilter('c.id', '=', @$_SESSION['filtreAdmin'][__FILE__]['id_commande']), 'AND',
        $req_build->setFilter('v.immatriculation', 'LIKE', array('%' . @$_SESSION['filtreAdmin'][__FILE__]['immatriculation'] . '%'), 'OR'), 'AND',
        $req_build->setFilter('v.chassis', 'LIKE', array('%' . @$_SESSION['filtreAdmin'][__FILE__]['chassis'] . '%'), 'OR'), 'AND',
        $req_build->setFilter('v.nom', 'LIKE', array('%' . @$_SESSION['filtreAdmin'][__FILE__]['modele'] . '%')), 'AND',
        $req_build->setFilter('c.date', '>=', @$_SESSION['filtreAdmin'][__FILE__]['date_debut']), 'AND',
        $req_build->setFilter('c.date', '<=', @$_SESSION['filtreAdmin'][__FILE__]['date_fin']), 'AND',
        $req_build->setFilter('total_ttc', '>=', @$_SESSION['filtreAdmin'][__FILE__]['total_debut']), 'AND',
        $req_build->setFilter('total_ttc', '<=', @$_SESSION['filtreAdmin'][__FILE__]['total_fin']), 'AND',
        $req_build->setFilter('id_site', '=', @$_SESSION['filtreAdmin'][__FILE__]['id_site']), 'AND',
        $req_build->setFilter('v.id_financeur', '=', @$_SESSION['filtreAdmin'][__FILE__]['id_financeur']), 'AND',
        $req_build->setFilter('v.id_concessionnaire', '=', @$_SESSION['filtreAdmin'][__FILE__]['id_concessionnaire']), 'AND',
        //$req_build->setFilter('cli.nom||cli.prenom||cli.societe','LIKE',array('%'.@$_SESSION['filtreAdmin'][__FILE__]['client'].'%')),'AND',
        $req_build->setFilter('cli.id', '=', @$_SESSION['filtreAdmin'][__FILE__]['id_client']), 'AND',
        $req_build->setFilter('cee.id_commande_etat', '=', @$_SESSION['filtreAdmin'][__FILE__]['id_commande_etat']), 'AND',
        $req_build->setFilter('v.is_delivred', '=', @$_SESSION['filtreAdmin'][__FILE__]['is_delivred']), 'AND',
        $req_build->setFilter('v.id_extension', '!=', '0'), 'AND',
        $req_build->setFilter('c.valide', '=', @$_SESSION['filtreAdmin'][__FILE__]['valide']), 'AND',
        $req_build->setFilter('cli.societe', 'LIKE', '%'.@$_SESSION['filtreAdmin'][__FILE__]['client'].'%')
    );


    //var_dump($cdes_conditions['conditions']);

    /* OLD INTERFACE
	Loader::loadClass('Commande_DetailCommande_Liste_ListeDetailCommande',PATH_CLASSES);
	$liste_cmd = new Commande_DetailCommande_Liste_ListeDetailCommande(true);
	$liste_cmd->setPageCourante($page);
	$liste_cmd->setNbResultatsPage($limite);
	$liste_cmd->addFiltre('valide','1');
	if($type != 'contrat')
		$liste_cmd->addFiltre('cit.is_delivred',0);
	else
		$liste_cmd->addFiltre('cit.is_delivred',1);
		$liste_cmd->addOrdre('c.date','DESC');

	if(count($_SESSION['filtreAdmin'][__FILE__]) AND $form->isValid($_SESSION['filtreAdmin'][__FILE__])){

		if(isset($_SESSION['filtreAdmin'][__FILE__]['id_commande']) AND !empty($_SESSION['filtreAdmin'][__FILE__]['id_commande']))
			$liste_cmd->addFiltre('c.id',$id_commande->getValue(),'=');
		if(!empty($_SESSION['filtreAdmin'][__FILE__]['date_debut']) AND !empty($_SESSION['filtreAdmin'][__FILE__]['date_fin']))
			$liste_cmd->addFiltre('c.date',array(dateToTimestamp($date_debut->getValue()),dateToTimestamp($date_fin->getValue())),'between');
		if(isset($_SESSION['filtreAdmin'][__FILE__]['total_debut']) AND !empty($_SESSION['filtreAdmin'][__FILE__]['total_debut']))
			$liste_cmd->addFiltre('c.total_ttc',$total_debut->getValue(),'>=');
		if(isset($_SESSION['filtreAdmin'][__FILE__]['total_fin']) AND !empty($_SESSION['filtreAdmin'][__FILE__]['total_fin']))
			$liste_cmd->addFiltre('c.total_ttc',$total_fin->getValue(),'<=');
		if(isset($_SESSION['filtreAdmin'][__FILE__]['etat']) AND $_SESSION['filtreAdmin'][__FILE__]['etat'] != 'all')
			$liste_cmd->addFiltre('cee.id_commande_etat',$etat->getValue());
		if(isset($_SESSION['filtreAdmin'][__FILE__]['id_site']) AND $_SESSION['filtreAdmin'][__FILE__]['id_site'] != 'all')
			$liste_cmd->addFiltre('c.id_site',$id_site->getValue());
		if(isset($_SESSION['filtreAdmin'][__FILE__]['client']) AND $_SESSION['filtreAdmin'][__FILE__]['client'] != '')
			$liste_cmd->addFiltre('cl.societe','%'.$client->getValue().'%','LIKE');
		if(isset($_SESSION['filtreAdmin'][__FILE__]['immatriculation']) AND $_SESSION['filtreAdmin'][__FILE__]['immatriculation'] != '')
			$liste_cmd->addFiltre('cit.immatriculation','%'.$immatriculation->getValue().'%','LIKE');
		if(!empty($_SESSION['filtreAdmin'][__FILE__]['id_concessionnaire']) AND $_SESSION['filtreAdmin'][__FILE__]['id_concessionnaire'] != 'all')
			$liste_cmd->addFiltre('cit.id_concessionnaire',$id_concessionnaire->getValue());
		if(!empty($_SESSION['filtreAdmin'][__FILE__]['id_financeur']) AND $_SESSION['filtreAdmin'][__FILE__]['id_financeur'] != 'all')
			$liste_cmd->addFiltre('cit.id_financeur',$id_financeur->getValue());
		if(!empty($_SESSION['filtreAdmin'][__FILE__]['date_prevue_debut']) AND !empty($_SESSION['filtreAdmin'][__FILE__]['date_prevue_fin']))
			$liste_cmd->addFiltre('cit.date_livraison_prev',array(dateToTimestamp($date_prevue_debut->getValue()),dateToTimestamp($date_prevue_fin->getValue())),'between');
		if(isset($_SESSION['filtreAdmin'][__FILE__]['modele']) AND $_SESSION['filtreAdmin'][__FILE__]['modele'] != '')
			$liste_cmd->addFiltre('cit.nom','%'.$modele->getValue().'%','LIKE');
	}

	if( in_array($type,array('order','contrat')))
	{
		$liste_cmd->addFiltre('cee.id_commande_etat',2);
	}
	else
	{
		$liste_cmd->addFiltre('cee.id_commande_etat',1);
	}

	$liste_cmd->setLimite($liste_cmd->getResultatCourant(),$liste_cmd->getNbResultatsPage());
	$liste_cmd->getListeInfos();
	$liste_cmd->getNbResultats();
	*/


    //var_dump($cdes_conditions);
    //exit;
    $page = (isset($_GET['page']) & !empty($_GET['page'])) ? $_GET['page'] : 1;
    
    

    //$_GET['debug_querries']=1;
    $cdes = $mdl_cdes->findForeignResults($cdes_conditions, false, $page);
    //$_GET['debug_querries']=0;exit;
    //là on reprend les anbciennes classes à) Cyril pck c'est tellement bien fait
    //que pour retrouver le nom de l'option dune commande c'est la merde...
    $options = new Catalogue_Liste_ListeOption();
    $options->getListeInfos();
    $options->getListe();

    $opts_val_names = array();
    foreach ($options->getListe() as $opt) {
        if ($opt->getIdAttribut() == 4) {
            $opts_val_names['contrat'][$opt->getId()] = ucfirst(strtolower(htmlentities($opt->getNom()->val())));
        }
        if ($opt->getIdAttribut() == 2) {
            $opts_val_names['km'][$opt->getId()] = ($opt->getNom()->val());
        }
        if ($opt->getIdAttribut() == 1) {
            $opts_val_names['durees'][$opt->getId()] = ($opt->getNom()->val());
        }
    }


    //var_dump($cdes);
    //exit;

    //pour chaque commande on ira cherche les détails du 1er item
    //(1 item = une voiture avant et maintenant on considère que c une commade x fois le même véhicule)
    foreach ($cdes['results'] as $cde => $infos) {
        if (is_int(@$_SESSION['filtreAdmin'][__FILE__]['is_delivred']))
            $cdes['results'][$cde]['items'] = ($mdl_cdes->getCommandItems($infos['id_cde'], '*', $_SESSION['filtreAdmin'][__FILE__]['is_delivred']));
        else
            $cdes['results'][$cde]['items'] = ($mdl_cdes->getCommandItems($infos['id_cde'], '*', 0));

        $cdes['results'][$cde]['commande_reseaux'] = ($mdl_cdes->getCommandReseaux($infos['id_cde']));
        if (count($mdl_cdes->getCommandItems($infos['id_cde'], '*', 1)) > 0) {
            $cdes['results'][$cde]['has_vehicule_livred'] = true;
        } else {
            $cdes['results'][$cde]['has_vehicule_livred'] = false;
        }
        //$_GET['debug_querries']=0;
    }

    $datas = array();
    //$datas['form'] = $form;
    if (in_array($_SERVER['REMOTE_ADDR'], array('5.1.33.16', '::1'))) {
        $datas['query'] = $cdes_conditions;
    } else {
        $datas['query'] = '';
    }

    $datas['new_view'] = 'liveEditor';
    $datas['opts_val_names'] = $opts_val_names;
    $datas['reseaux_names'] = $mdl_reseaux->getResauxNames();
    $datas['list_cdes'] = $cdes['results'];
    $datas['pagination'] = $cdes['pagination'];
    $datas['liste_sites'] = 'sites';
    $datas['liste_etats'] = 'etats';
    $datas['dealers'] = $liste_dealers;
    $datas['financeurs'] = $liste_financeurs;
    $datas['loueurs'] = 'loueurs';
    $datas['cdes_etat'] = $type;
    $datas['filters_session_file'] = __FILE__;
    switch ($type) {
        case 'order':
            $datas['url'] = $app->urlFor('orders', array('type' => 'order'));
            $datas['title'] = 'Liste des Commandes';
            break;
        case 'contrat':
            $datas['url'] = $app->urlFor('orders', array('type' => 'contrat'));
            $datas['title'] = 'Liste des Commandes Livrées';
            break;
        default:
            $datas['url'] = $app->urlFor('orders', array('type' => 'estimate'));
            $datas['title'] = 'Liste des Devis';
            break;
    }

    /*var_dump(file_get_contents('/../views/order'.DIRECTORY_SEPARATOR.'list_orders_new.php'));
	exit;*/


    //$app->render('order'.DIRECTORY_SEPARATOR.'list2_new.php',$datas);
    $app->render('order' . DIRECTORY_SEPARATOR . 'list_orders_new.php', $datas);

})->name('orders');

/*
$app->get('/order2/vehicules/', function () use ($app) {


	$template = new Template();
	$template->useMasterLayout(false);
	$template->setScriptPath(PATH_TEMPLATE.'_helpers'.DIRECTORY_SEPARATOR);
	$template->liste = array("");
	$html_view = $template->render('nav.php');
	echo $html_view;

	$_GET['ajax']=true;
	$datas = array();
	$datas['is_modal'] = (isset($_GET['ajax'])) ? true : false;
	//$datas['edit_url'] = (!isset($_GET['ajax'])) ? $app->urlFor('vehicule_edit',array('id_order'=>$id_order,'id'=>$id)) : $app->urlFor('vehicule_edit',array('id_order'=>$id_order,'id'=>$id)).'?ajax';
	$app->render('order'.DIRECTORY_SEPARATOR.'details'.DIRECTORY_SEPARATOR.'vehicules.php',$datas);

})->name('orders2_vehicules');	*/

/* Vérifier si stickage */
$app->get('/order/has-stickage', function () use ($app) {

    $id_cmd = @$_REQUEST['id_cmd'];

    $mdl_cmd = Model::Load('Commande_ModelExtends_Commande');
    $hasStickage = $mdl_cmd->hasStickage($id_cmd);
    $return = $hasStickage ? '1' : '0';
    
    if ($return) {

        header('Content-Type: application/json');
        exit(json_encode(true));
    } else {
        header('Content-Type: application/json');
        exit(json_encode(false));
    }
})->via('GET', 'POST')->name('order_has_stickage');

/* passer un devis en commande */
$app->get('/order/change_etat', function () use ($app) {

    $id_cmd = @$_REQUEST['id_cmd'];
    $id_state = @$_REQUEST['id_state'];

    $mdl_cmd = Model::Load('Commande_ModelExtends_Commande');
    $mdl_cmd_etape_etat = Model::Load('Commande_ModelExtends_EtapeEtat');
    $mdl_cmd->id = $id_cmd;

    if (!$cmd_infos = $mdl_cmd->read()) {
        //exit('La commande n\'existe pas');
        //header('Location: '.$_SERVER['HTTP_REFERER']);
        header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found');
    }

    if ($mdl_cmd_etape_etat->setCommandeEtapeEtat($id_cmd, $id_state, $app->user->getId())) {
        //exit('PASSAGE COMMANDE SAUVEGARDE');
        header($_SERVER['SERVER_PROTOCOL'] . ' 200 OK');
        exit('Passée en commande');
    }
    exit;
})->via('GET', 'POST')->name('order_change_state');


/* route et vues véhicules des détails cmd */
$app->get('/order/vehicules-edit/(:id_cmd)(/:action/:id_item)', function ($id_cmd = null, $action = null, $id_item = null) use ($app) {
    $db = Mysql::getInstance();
    /*
    var_dump($_POST);
    var_dump($id_cmd);
    var_dump($action);
    var_dump($id_item);*/
    //enregistrement
    if (count($_REQUEST) > 1 || $action != null) {

        //modèles
        $mdl_cmd = Model::Load('Commande_ModelExtends_Commande');
        $mdl_cmd_items = Model::Load('Commande_ModelExtends_Item');
        $mdl_StatusCmd = Model::Load('StatusLive_StatusCmd');


        if ($action == 'duplicate') {
            header('Content-Type: application/json');
            $mdl_cmd_items->id = $id_item;
            if ($item_infos = $mdl_cmd_items->read()) {
                unset($item_infos['id']);

                //formatage immatriculation
                /*$item_infos['immatriculation'] = $item_infos['immatriculation'];
		if($immat = checkImmatriculation($item_infos['immatriculation'])) {
			$item_infos['immatriculation'] = $immat['immatriculation'];
		} else {
			$item_infos['immatriculation'] = '';
		}*/
                $item_infos['immatriculation'] = '';
                $item_infos['chassis'] = '';

                foreach ($item_infos as $k => $v) {
                    if (preg_match('#^date_(.*)$#', $k)) {
                        $item_infos[$k] = '';
                    }
                }


                if ($saved = $mdl_cmd_items->save($item_infos, null)) {
                    $mdl_cmd->reloadItemNumbers($id_cmd);
                    echo json_encode(array('success' => true));
                    exit;
                } else {
                    throw new Exception('Nous ne sommes pas parvenu à dupliquer le véhicule');
                    exit;
                }
            } else {
                throw new Exception('Nous ne sommes pas parvenu à identifier le véhicule à dupliquer');
                exit;
            }

            echo json_encode(array('success' => false));
            exit;

        } elseif ($action == 'delete') {

            $locked = $mdl_StatusCmd->StatusLock($id_cmd); //on tente de bloquer pour avoir la main
            if ($locked) {
                header('Content-Type: application/json');
                $mdl_cmd_items->id = $id_item;
                if ($item_infos = $mdl_cmd_items->read()) {
                    if ($sdeleted = $mdl_cmd_items->del($mdl_cmd_items->id)) {

                        ///azjouter le cas où y'a plus d'items ne pas réordonner
                        @$mdl_cmd->reloadItemNumbers($id_cmd);
                        $mdl_cmd->reloadItemNumbers($id_cmd);
                        echo json_encode(array('success' => true));
                        exit;
                    } else {
                        throw new Exception('Nous ne sommes pas parvenu à supprimer le véhicule');
                        exit;
                    }
                } else {
                    throw new Exception('Nous ne sommes pas parvenu à identifier le véhicule à supprimer');
                    exit;
                }
                echo json_encode(array('success' => false));
                exit;

            } else {
                throw new Exception('Nous ne sommes pas parvenu à vous laisser la main');
                exit;
            }

        } else { //enregistrement d'une modif
            $errors = array();
            $locked = $mdl_StatusCmd->StatusLock($id_cmd); //on tente de bloquer pour avoir la main
            if ($locked) {
                $ids_cmd_items = array_keys(@$_REQUEST['immat']);
		/*var_dump($ids_cmd_items);
		var_dump($_REQUEST);*/
                $bdd_keys = array(
                    'id_concessionnaire' => 'id_concessionnaire',
                    'id_financeur' => 'id_financeur',
                    'date_env_contrat' => 'date_env_contrat',
                    'date_rec_contrat' => 'date_rec_contrat',
                    'date_livraison_prev' => 'date_livraison_prev',
                    'date_livraison_eff' => 'date_livraison',
                    'date_retour_prev' => 'date_retour_prev',
                    'date_retour_eff' => 'date_retour',
                    'delivred' => 'is_delivred',
                    'is_sous_loc' => 'is_sous_loc',
                    'immat' => 'immatriculation',
                    'chassis' => 'chassis');

                $time_keys = array(
                    'date_env_contrat',
                    'date_rec_contrat',
                    'date_livraison_prev',
                    'date_livraison_eff',
                    'date_retour_prev',
                    'date_retour_eff');

                $infos = null;
                //formatage des dates
                foreach ($ids_cmd_items as $id_item) {
                    $infos = null;

                    //pour chaque clef
                    foreach ($bdd_keys as $form_key => $BDD_key) {

                        if (isset($_REQUEST[$form_key][$id_item])) {

                            //formatage timestamps
                            if (in_array($form_key, $time_keys)) {			    
                                if ($_REQUEST[$form_key][$id_item] != '') $_REQUEST[$form_key][$id_item] = dateToTime($_REQUEST[$form_key][$id_item]);
                                else $_REQUEST[$form_key][$id_item] = 0;
                            }
                            //formatage financeur et concessionnaire
                            if (@$_REQUEST['id_concessionnaire'][$id_item] == '') $_REQUEST['id_concessionnaire'][$id_item] = 0;
                            if (@$_REQUEST['id_financeur'][$id_item] == '') $_REQUEST['id_financeur'][$id_item] = 0;
                        }
                    }

                    //controle des champs de dates
                    if (!empty($_REQUEST['date_env_contrat'][$id_item]) && !empty($_REQUEST['date_rec_contrat'][$id_item]) && $_REQUEST['date_rec_contrat'][$id_item] < $_REQUEST['date_env_contrat'][$id_item]) {
                        $mdl_cmd_items->id = $id_item;
                        $item_number = $mdl_cmd_items->read('item_order');
                        if ($item_number['item_order'] == 0) $item_number['item_order'] = 1;
                        $errors[] = _('La date de réception de contrat du véhicule n°' . $item_number['item_order'] . ' ne peut-être antérieure à la date d\'envoi du contrat');
                        unset($_REQUEST['date_env_contrat'][$id_item]);
                        unset($_REQUEST['date_rec_contrat'][$id_item]);
                    }

                    if (!empty($_REQUEST['date_rec_contrat'][$id_item]) && !empty($_REQUEST['date_livraison_prev'][$id_item]) && $_REQUEST['date_livraison_prev'][$id_item] < $_REQUEST['date_rec_contrat'][$id_item]) {
                        $mdl_cmd_items->id = $id_item;
                        $item_number = $mdl_cmd_items->read('item_order');
                        if ($item_number['item_order'] == 0) $item_number['item_order'] = 1;
                        $errors[] = _('La date de livraison prévue du véhicule n°' . $item_number['item_order'] . ' ne peut-être antérieure à la date de réception du contrat');
                        unset($_REQUEST['date_rec_contrat'][$id_item]);
                        unset($_REQUEST['date_livraison_prev'][$id_item]);
                    }
                    
                    
                    $mdl_cmd_item = Model::Load('Commande_ModelExtends_Item');
				    $mdl_cmd_item->id = $id_item;

				    if (!$mdl_cmd_item->read()) {
				        throw new Exception('Le véhicule n\'existe pas !');
				        exit();
				    }

				    $infos = $mdl_cmd_item->read();


				    $date_livraison_ant = $infos['date_livraison'];
				    $date_livraison_new = $_REQUEST['date_livraison_eff'][$id_item];

				    if($date_livraison_new){
				    	if(empty($date_livraison_ant)){
                    		$mdl_cmd_item->save(array('date_entree_date_livraison' => date("Y-m-d H:i:s")), $mdl_cmd_item->id);
				    	}else if($date_livraison_ant == $date_livraison_new){
				    		///nothing
				    	}else if( !empty($date_livraison_ant) && empty($date_livraison_new)){
				    		$mdl_cmd_item->save(array('date_efface_date_livraison' => date("Y-m-d H:i:s")), $mdl_cmd_item->id);
				    	}else{
				    		///modif
				    		$mdl_cmd_item->save(array('date_modif_date_livraison' => date("Y-m-d H:i:s")), $mdl_cmd_item->id);
				    	}
				    }else{
						if(empty($date_livraison_ant)){
							/// nothing
						}else if (!empty($date_livraison_ant) && empty($date_livraison_new)){
							$mdl_cmd_item->save(array('date_efface_date_livraison' => date("Y-m-d H:i:s")), $mdl_cmd_item->id);
						}
				    }      
                }

                $isLivred = false;
                //pour chaque item on va save les infos
                foreach ($ids_cmd_items as $id_item) {

                    $infos = null;
                    //pour chaque clef
                    foreach ($bdd_keys as $form_key => $BDD_key) {

                        if (isset($_REQUEST[$form_key][$id_item])) {
                            $infos[$BDD_key] = trim($_REQUEST[$form_key][$id_item]);
                            //echo $BDD_key."/".$form_key." = ".$_REQUEST[$form_key][$id_item]."<br />";
                        }

                        //formatage immatriculation
                        if ($form_key == 'immat') {
                            $immat = preg_replace('/\s|\-/', '', $_REQUEST[$form_key][$id_item]);
                            if ($immat != '') {
                                if ($immat = checkImmatriculation($_REQUEST[$form_key][$id_item])) {

                                    $item = $mdl_cmd_items->find(array('conditions' => array('immatriculation=' => $immat['immatriculation'], 'id!=' => $id_item, 'is_rerented=' => 0, 'is_delivred='=>0, 'is_avenant='=>0)));
                                    if (empty($item)) {
                                        $infos[$BDD_key] = $immat['immatriculation'];
                                    } else {
                                        preparerNotification(_('L\'imatriculation ' . $immat['immatriculation'] . ' est déjà assignée à un autre véhicule'), 'error', true);
                                        $infos[$BDD_key] = '';

                                    }
                                } else {
                                    $errors[] = _('Le format de l\'imatriculation ' . $immat . ' n\'est pas valide');
                                    //preparerNotification(_('le format de l\'imatriculation '.$immat.' n\'est pas valide'), 'error', true);
                                    $infos[$BDD_key] = '';
                                }
                            }
                        }


                        //formatage chassis
                        if ($form_key == 'chassis') {
                            $chassis = preg_replace('/\s/', '', $_REQUEST[$form_key][$id_item]);
                            if (strlen($chassis) == 17) {
                                $item = $mdl_cmd_items->find(array('conditions' => array('chassis=' => $chassis, 'id!=' => $id_item, 'is_rerented=' => 0, 'is_delivred='=>0,'is_avenant='=>0)));
                                if (empty($item)) {
                                    $infos[$BDD_key] = trim($_REQUEST[$form_key][$id_item]);
                                } else {
                                    $errors[] = _('Le chassis n° ' . $chassis . ' est déjà assignée à un autre véhicule');
                                    //preparerNotification(_('Le chassis n° '.$chassis.' est déjà assignée à un autre véhicule'), 'error', true);
                                    $infos[$BDD_key] = '';
                                }
                            } else {
                                if (trim($_REQUEST[$form_key][$id_item]) !== '') {
                                    $errors[] = _('Le format du chassis n° ' . $chassis . ' n\'est pas valide');
                                    //preparerNotification(_('Le chassis n° '.$chassis.' est déjà assignée à un autre véhicule'), 'error', true);
                                    $infos[$BDD_key] = '';
                                }
                            }
                        }
                        /*$_GET['debug_querries']=1;
                var_dump($id_item);
                var_dump($infos);
                echo "<br />================================<br />";*/
                        if (
                            array_key_exists('immatriculation', $infos)
                            && array_key_exists('chassis', $infos)
                            && array_key_exists('is_sous_loc', $infos)
                            && array_key_exists('date_livraison', $infos)
                            && array_key_exists('is_delivred', $infos)
                        ) {
                            if (
                                $infos['immatriculation'] != ''
                                && $infos['chassis'] != ''
                                && ($infos['is_sous_loc'] == 1 || $infos['is_sous_loc'] == 0 || $infos['is_sous_loc'] == 2)
                                && $infos['date_livraison'] != ''
                                && $infos['is_delivred'] == '1'
                            ) {
                                $infos['is_delivred'] = '1';
                                $isLivred = true;
                            } else {
                                $infos['is_delivred'] = '0';
                            }

                        }
                    }
                    if ( $isLivred || ($infos['date_livraison'] != '' && $infos['date_livraison'] != "0" ) ) {
                        $zDateLivraison = date('Y-m-d', $infos['date_livraison']);
                        list($zYLivraison, $zMLivraison, $zDLivraison) = explode('-', $zDateLivraison);
                        $zDateRetourPrev = mktime(0, 0, 0, $zMLivraison * 1 + $_REQUEST['duree'] * 1, $zDLivraison, $zYLivraison);
                        $infos['date_retour_prev'] = $zDateRetourPrev;
                        $infos['date_saisie_livraison'] = time();
                        $db->sqlQuery(query_update_prep("commandes", array('date' => time()), "id='" . $id_cmd . "'"));
                    } 

                    if (!$mdl_cmd_items->save($infos, $id_item)) {
                        throw new Exception('Nous ne sommes pas parvenu à sauvegarder l\'intégralité du tableau des véhicules');
                        exit;
                    }

                }
                //exit;
                $mdl_cmd->reloadItemNumbers($id_cmd);
                $mdl_StatusCmd->StatusUnlock($id_cmd); //on laisse la main

                $zReferer = @$_REQUEST['hreferer'] != '' ? @$_REQUEST['hreferer'] : @$_SERVER['HTTP_REFERER'];
                $url_parsed = parse_url($zReferer);
                //$redir = $url_parsed['scheme'].'://'.$url_parsed['host'].$url_parsed['path'].$_SERVER['QUERY_STRING'];

                if (@$_REQUEST['from-ajax'] != true) {
                    if (count($errors) > 0) {
                        preparerNotification('<strong>Modifications enregistrées avec des erreurs :</strong><br/><br/> - ' . implode('<br/> - ', $errors), 'warning', true);
                    } else {
                        preparerNotification(_("Succès"), 'success');
                    }
                }


                if (@$_REQUEST['from-ajax'] == true) {
                    echo $id_cmd;
                } else {
                    header('Location: ' . $zReferer . '#cde_' . $id_cmd);
                }

                //var_dump($redir);
                exit();

                header('Location: ' . $zReferer);
                //header('Location: '.$app->urlFor('order_edit',array('id'=>$id_cmd)));
                exit;

            } else {
                throw new Exception('Nous ne sommes pas parvenu à vous laisser la main');
                exit;
            }
        }
    }
    //exit;
    //vue
    if ($id_cmd != null) {
        //var_dump($id_cmd);
        $mdl_cmd = Model::Load('Commande_ModelExtends_Commande');
        $mdl_dealers = Model::Load('Catalogue_ModelExtends_Dealer');


        $datas = array();
        $datas['id_cmd'] = $id_cmd;
        $datas['cmd_status'] = $mdl_cmd->getCommandeEtapeEtat($id_cmd)['id_commande_etat'];
        $datas['items'] = $mdl_cmd->getCommandItems($id_cmd);
        $datas['items_contrats'] = $mdl_cmd->getInfosContratCommand($id_cmd);
        $datas['dealers'] = $mdl_dealers->getDealerInfo(array());
        $datas['new_view'] = 'liveEditor';

        $mdl_financeurs = Model::Load('Catalogue_ModelExtends_Financeur');
        //LISTES COMMUNES (non pas communes de villes en commun sur les cdes quoi)
        $liste_financeurs = $mdl_financeurs->getFinanceursInfo(array());
        $datas['financeurs'] = $liste_financeurs;
        $app->view->useMasterLayout(false);
        /*
		if(!isset($_GET['simple'])) {
			$app->render('vehicule'.DIRECTORY_SEPARATOR.'_list.php',$datas);
		} else {
			$app->render('vehicule'.DIRECTORY_SEPARATOR.'_list_simple.php',$datas);
		}
	*/

        if (@$mdl_cmd->getCommandeEtapeEtat($id_cmd)['id_commande_etat'] == 1) {
            $app->render('vehicule' . DIRECTORY_SEPARATOR . 'vehicules_details_devis.php', $datas);
        } else {
            if (@$_REQUEST['view'] == 'contrat') {
                $app->render('vehicule' . DIRECTORY_SEPARATOR . 'vehicules_details_contrat.php', $datas);
            } else {
                $app->render('vehicule' . DIRECTORY_SEPARATOR . 'vehicules_details_order.php', $datas);
            }
        }
        exit;
    }


})->via('GET', 'POST')->name('order_vehicules_edit');


$app->get('/order/sous-loc/', function () use ($app) {
    $mdl_cmd = Model::Load('Commande_ModelExtends_Commande');
    $mdl_cmd->id = $_REQUEST['id_cmd'];
    if (!$mdl_cmd->read()) {
        throw new Exception('La commande n\'existe pas !');
        exit();
    }
    if ($mdl_cmd->SetItemsSousLoc($_REQUEST['id_cmd'], $_REQUEST['value'])) {
        header('Content-Type: application/json');
        exit(json_encode(true));
    } else {
        header('Content-Type: application/json');
        exit(json_encode(false));
    }
})->via('GET', 'POST')->name('order_sous_loc');

/* route et vues véhicules des détails cmd */
$app->get('/order/vehicules-list/(:id_cmd)(/:action/:id_item)', function ($id_cmd = null, $action = null, $id_item = null) use ($app) {


    //il faut une commande
    if ($id_cmd == null) {
        header('HTTP/1.0 404 Not Found');
        echo('<h3 class="text-danger">Cette commande n\'existe pas ou plus</h3>');
        exit;
    }

    //MODELS
    $mdl_cmd = Model::Load('Commande_ModelExtends_Commande');
    $mdl_cmd_items = Model::Load('Commande_ModelExtends_Item');
    $mdl_StatusCmd = Model::Load('StatusLive_StatusCmd');
    $mdl_financeurs = Model::Load('Catalogue_ModelExtends_Financeur');

    //LISTES COMMUNES (non pas communes de villes en commun sur les cdes quoi)
    $liste_financeurs = $mdl_financeurs->getFinanceursInfo(array('order' => 'societe'));


    //
    $mdl_cmd->id = $id_cmd;
    if ($id_cmd != null & !$mdl_cmd->read()) {
        exit('<span class="label label-danger">La commande n\'existe plus !</span>');
    }

    /*
    var_dump($_POST);
    var_dump($id_cmd);
    var_dump($action);
    var_dump($id_item);
    */

    //enregistrement
    if (count($_POST) || $action != null) {

        if ($action == 'duplicate') {
            header('Content-Type: application/json');
            $mdl_cmd_items->id = $id_item;
            if ($item_infos = $mdl_cmd_items->read()) {
                unset($item_infos['id']);

                //formatage immatriculation & chassis
                $item_infos['immatriculation'] = '';
                $item_infos['chassis'] = '';

                if ($saved = $mdl_cmd_items->save($item_infos, null)) {
                    $mdl_cmd->reloadItemNumbers($id_cmd);
                    echo json_encode(array('success' => true));
                    exit;
                } else {
                    throw new Exception('Nous ne sommes pas parvenu à dupliquer le véhicule');
                    exit;
                }
            } else {
                throw new Exception('Nous ne sommes pas parvenu à identifier le véhicule à dupliquer');
                exit;
            }

            echo json_encode(array('success' => false));
            exit;

        } elseif ($action == 'delete') {

            $locked = $mdl_StatusCmd->StatusLock($id_cmd); //on tente de bloquer pour avoir la main
            if ($locked) {
                header('Content-Type: application/json');
                $mdl_cmd_items->id = $id_item;
                if ($item_infos = $mdl_cmd_items->read()) {
                    if ($sdeleted = $mdl_cmd_items->del($mdl_cmd_items->id)) {
                        @$mdl_cmd->reloadItemNumbers($id_cmd);
                        echo json_encode(array('success' => true));
                        exit;
                    } else {
                        throw new Exception('Nous ne sommes pas parvenu à supprimer le véhicule');
                        exit;
                    }
                } else {
                    throw new Exception('Nous ne sommes pas parvenu à identifier le véhicule à supprimer');
                    exit;
                }
                echo json_encode(array('success' => false));
                exit;

            } else {
                throw new Exception('Nous ne sommes pas parvenu à vous laisser la main');
                exit;
            }

        } else { //enregistrement d'une modif

            $locked = $mdl_StatusCmd->StatusLock($id_cmd); //on tente de bloquer pour avoir la main
            if ($locked) {

                $ids_cmd_items = array_keys($_POST['concessionnaire']);
                $bdd_keys = array(
                    'concessionnaire' => 'id_concessionnaire',
                    'date_livraison_prev' => 'date_livraison_prev',
                    'date_livraison' => 'date_livraison',
                    'date_retour_prev' => 'date_retour_prev',
                    'date_retour' => 'date_retour',
                    'delivered' => 'is_delivred',
                    'immat' => 'immatriculation',
                    'chassis' => 'chassis');

                $time_keys = array('date_livraison_prev',
                    'date_livraison',
                    'date_retour_prev',
                    'date_retour');

                $infos = null;
                //pour chaque item on va save les infos
                foreach ($ids_cmd_items as $id_item) {
                    $infos = null;
                    //pour chaque clef
                    foreach ($bdd_keys as $form_key => $BDD_key) {

                        if (in_array($form_key, $time_keys)) {
                            $infos[$BDD_key] = '';
                            if ($_POST[$form_key][$id_item] != '') $infos[$BDD_key] = dateToTime($_POST[$form_key][$id_item]);
                        } else {
                            $infos[$BDD_key] = $_POST[$form_key][$id_item];
                        }

                        //formatage immatriculation
                        if ($form_key == 'immat') {
                            $infos[$BDD_key] = $_POST[$form_key][$id_item];
                            if ($immat = checkImmatriculation($_POST[$form_key][$id_item])) {
                                $infos[$BDD_key] = $immat['immatriculation'];
                            } else {
                                $infos[$BDD_key] = '';
                            }
                        }
                        if ($form_key == 'chassis') {
                            $infos[$BDD_key] = preg_replace('/\s/', '', $_POST[$form_key][$id_item]);
                        }
                    }
                    //var_dump($infos);
                    if (!$mdl_cmd_items->save($infos, $id_item)) {
                        throw new Exception('Nous ne sommes pas parvenu à sauvegarder l\'intégralité du tableau des véhicules');
                        exit;
                    }
                }

                $mdl_cmd->reloadItemNumbers($id_cmd);
                $mdl_StatusCmd->StatusUnlock($id_cmd); //on laisse la main

                exit;
                /*
		header('Content-Type: text/html; charset=utf-8');
		var_dump(currURL());
		exit;
		*/
                header('Location: ' . $app->urlFor('order_edit', array('id' => $id_cmd)));
                exit;

            } else {
                throw new Exception('Nous ne sommes pas parvenu à vous laisser la main');
                exit;
            }
        }
    }


    //vue
    if ($id_cmd != null) {

        //var_dump($id_cmd);
        $mdl_cmd = Model::Load('Commande_ModelExtends_Commande');
        $mdl_dealers = Model::Load('Catalogue_ModelExtends_Dealer');

        $datas = array();
        $datas['id_cmd'] = $id_cmd;
        $datas['cmd_status'] = $mdl_cmd->getCommandeEtapeEtat($id_cmd)['id_commande_etat'];

        $iType = 0;
        $zUrl = $_SERVER['HTTP_REFERER'];
        if (strpos($zUrl, 'order/contrat')) {
            $iType = 1;
        }
        $datas['items'] = $mdl_cmd->getCommandItems($id_cmd, '*', $iType);

        $datas['dealers'] = $mdl_dealers->getDealerInfo();
        $datas['new_view'] = 'liveEditor';
        $datas['financeurs'] = $liste_financeurs;
        //var_dump($datas['items']);
        //exit;
        $app->view->useMasterLayout(false);
        /*
		if(!isset($_GET['simple'])) {
			$app->render('vehicule'.DIRECTORY_SEPARATOR.'_list.php',$datas);
		} else {
			$app->render('vehicule'.DIRECTORY_SEPARATOR.'_list_simple.php',$datas);
		}
	*/

        if ($_REQUEST['view'] == 'contrat') {
            $app->render('vehicule' . DIRECTORY_SEPARATOR . 'vehicules_list_contrats.php', $datas);
        } else {
            $app->render('vehicule' . DIRECTORY_SEPARATOR . 'vehicules_list_orders.php', $datas);
        }

        exit;
    }


})->via('GET', 'POST')->name('order_vehicules_list');


$app->get('/order/vehicules/(:id)', function ($id = null) use ($app) {

    if ($id != null) {
        $order = new Commande_DetailCommande($id);
		
        $order->getInfos();
//if ( isset($_GET["debug"]) ) {var_dump($order->getInfos());exit;}
        $datas = array();
        $datas['order'] = $order;
        $datas['is_ajax'] = true;
        $datas['new_view'] = false;

        $app->view->useMasterLayout(false);

        if (!isset($_GET['simple']))
            $app->render('vehicule' . DIRECTORY_SEPARATOR . '_list.php', $datas);
        else
            $app->render('vehicule' . DIRECTORY_SEPARATOR . '_list_simple.php', $datas);
    }

})->name('order_vehicules_old');


/* Documents complementaires*/
$app->get('/order/documents/(:id)/(:action)', function ($id_order, $action, $filename = null) use ($app) {

    $mdl_UploadDocs = Model::Load('Upload_UploadDocs');
    switch ($action) {
        case 'read':
            $files = $mdl_UploadDocs->readDir($id_order);
            break;

        case 'upload':
            $files = $mdl_UploadDocs->uploadFiles($id_order, $_FILES['docs']);
            break;

        case 'trash':
            $files = $mdl_UploadDocs->deleteFiles($id_order, array($_POST['filename']));
            break;

        case 'open':
            return $mdl_UploadDocs->openFile($id_order, $_POST['filename']);
            break;

        default:
            throw new Exception('Wrong action');
    }

    header('Content-Type: application/json');
    echo json_encode($files);
    exit;

})->via('GET', 'POST', 'FILES')->name('order_documents');

/* route des détails d'une commande */
$app->get('/order/edit/(:id)(/:new_view)', function ($id = null, $new_view = null) use ($app) {
    $db = Mysql::getInstance();

    //MODELS
    $mdl_cmd_reseaux = Model::Load('Commande_ModelExtends_Reseau');

    //Caractéristiques Options
    $caracteristiques_options = new Catalogue_Liste_ListeCaracteristique();
    $caracteristiques_options->addFiltre('c.is_show_admin', 1, '=');
    $caracteristiques_options->getListeInfos();
    $caracteristiques_options->getListe();

    //récupération pour affichage checkbox si il y a perte financière
    $mdl_cmd = Model::Load('Commande_ModelExtends_Commande');
    $isCmdPF = false;
    if ($id != null)
        $isCmdPF = $mdl_cmd->isCommandPF($id);


    $liste_reseaux = new Catalogue_Liste_ListeReseau();
    $liste_reseaux->addFiltre('r.is_actif', 1, '=');
    $liste_reseaux->getListe();

    $order = null;
    $etat = null;

    $req_sites = $db->sqlQuery("SELECT * FROM sites");
    $sites = array();

    if ($db->sqlNumRows($req_sites)) {
        while ($info = $db->sqlAssoc($req_sites)) {
            $sites[$info['id']] = $info;
        }
    }

    Loader::loadClass('Commande_Etat_Liste_ListeEtat', PATH_CLASSES);

    $liste_etats = new Commande_Etat_Liste_ListeEtat();
    $liste_etats->addOrdre('ce.position', 'ASC');
    $liste_etats->getListeInfos();


    /* === FORMULAIRE VEHICULE === */
    $form = new Form();

    $id_etat = $form->createElement('id_etat');
    $id_etat->setValueDefaut(1);
    $id_etat->addValidator('NotEmpty');
    $id_etat->addValidator('NumInt');
    
    $commentaire_pdf = $form->createElement('commentaire_pdf');
    $commentaire_pdf->setValueDefaut("");

    $reseau = $form->createElement('reseau');
    //$reseau -> addValidator('NumInt');
    //$reseau -> setIsArray(true);

    if (isset($_GET['id_customer']) AND $_GET['id_customer'] != null) {
        $client = new User_Client((int)$_GET['id_customer']);
        $client->getInformations();

        if (count($client->getReseaux())) {
            $reseau->setValueDefaut($client->getReseauxById());
        }
    }else if(!empty($id)){
    	Loader::loadClass('Commande_Commande', PATH_CLASSES);
        $commande = new Commande_Commande($id);
        $commande->getInformations();

    	$client = new User_Client($commande->getInformation('id_client'));
        $client->getInformations();

        if (count($client->getReseaux())) {
            $reseau->setValueDefaut($client->getReseauxById());
        }
    }
    if (!isset($_POST)) {
        Loader::loadClass('Commande_DetailCommande', PATH_CLASSES);
        $listeCommande = new Commande_DetailCommande($id);
        $listeCommande->getInfos();

        $reseau->setValueDefaut(explode(',', $listeCommande->getInformation('id_reseaux')));
    }

    $id_site = $form->createElement('id_site');
    $id_site->addValidator('NumInt');
    $id_site->setValueDefaut(1);

    $commentaire_pdf = $form->createElement('commentaire_pdf');

        
    $total_ht = $form->createElement('total_ht');
    $total_ht->addValidator('NumFloat');

    $id_client = $form->createElement('id_client');
    $id_client->addValidator('NotEmpty');
    $id_client->addValidator('NumInt');
    if (isset($_GET['id_customer']) AND $_GET['id_customer'] != null) {
        $id_client->setValueDefaut($_GET['id_customer']);
    }

    $date = $form->createElement('date');
    $date->addValidator('NotEmpty');
    $date->addValidator('Date');
    $date->setValueDefaut(date('d/m/Y', time()));

    $commentaire = $form->createElement('commentaire');
    $commentaire->setValueDefaut("");

    $id_order = $form->createElement('id_order');
    //$form -> addElements(array());

    $id_produit = $form->createElement('id_produit');
    $id_produit->addValidator('NotEmpty');
    $id_produit->addValidator('NumInt');

    $id_type_contrat = $form->createElement('id_type_contrat');
    $id_type_contrat->addValidator('NotEmpty');
    $id_type_contrat->addValidator('NumInt');

    $duree = $form->createElement('duree');
    $duree->addValidator('NotEmpty');
    $duree->addValidator('NumInt');

    $km = $form->createElement('km');
    $km->addValidator('NotEmpty');
    $km->addValidator('NumInt');

    $km_moyen = $form->createElement('km_moyen');
    $km_moyen->addValidator('NumInt');
	
	$periodicite = $form->createElement('periodicite');

    $duree_moyenne = $form->createElement('duree_moyenne');
    $duree_moyenne->addValidator('NumInt');

    $km_custom = $form->createElement('km_custom');
    $km_custom->addValidator('NumInt');

    $qte = $form->createElement('qte');
    $qte->addValidator('NumInt');
	
	$km_actuel = $form->createElement('km_actuel');
    $km_actuel->addValidator('NumInt');

    $nom = $form->createElement('nom');
    //Champs libres
    $champ_libre_label_1 = $form->createElement('champ_libre_label_1');
    $champ_libre_label_2 = $form->createElement('champ_libre_label_2');
    $champ_libre_valeur_1 = $form->createElement('champ_libre_valeur_1');
    $champ_libre_valeur_2 = $form->createElement('champ_libre_valeur_2');

    $prix_unitaire_ht = $form->createElement('prix_unitaire_ht');
    $prix_unitaire_ht->addValidator('NotEmpty');
    $prix_unitaire_ht->addValidator('NumFloat');

    $supplements = $form->createElement('supplements');
    $supplements->setIsArray(true);
    // $supplements -> addValidator('NumInt');

    $prix_supplements = $form->createElement('prix_supplements');
    $prix_supplements->setIsArray(true);
    // $prix_supplements -> addValidator('NumInt');

    $total_ht = $form->createElement('total_ht');
    $total_ht->addValidator('NumFloat');

    $com_concess = $form->createElement('com_concess');
    $com_concess->addValidator('NumFloat');

    $prime_gamme = $form->createElement('prime_gamme');
    $prime_gamme->addValidator('NumFloat');

    $prime_volume = $form->createElement('prime_volume');
    $prime_volume->addValidator('NumFloat');

    $com_loueur = $form->createElement('com_loueur');
    $com_loueur->addValidator('NumFloat');

    $couleur_perso = $form->createElement('couleur_perso');
    $couleur_perso->addValidator('StringLength', array(1, 255));

    $form->addElements(array(
        $id_produit,
        $id_type_contrat,
        $duree,
        $qte,
        $km,
        $km_moyen,
        $duree_moyenne,
        $km_custom,
        $nom,
        $prix_unitaire_ht,
        $supplements,
        $prix_supplements,
        $total_ht,
        $com_concess,
        $prime_gamme,
        $prime_volume,
        $com_loueur,
        $couleur_perso,
        $champ_libre_label_1,
        $champ_libre_label_2,
        $champ_libre_valeur_1,
        $champ_libre_valeur_2,
        $id_order,
        $total_ht,
        $reseau,
        $id_client,
        $commentaire,
        $date,
        $id_site,
        $commentaire_pdf,
        $id_etat,
		$periodicite,
		$km_actuel,
		$commentaire_pdf
    ));


    /* === FORMULAIRE MAILS === */
    $form_message = new Form();

    $message = $form_message->createElement('message');

    $email = $form_message->createElement('email');
    $email->addValidator('NotEmpty');
    $email->addValidator('AdresseEmail');

    $type_email = $form_message->createElement('type_email');
    $type_email->addValidator('NotEmpty');
    $type_email->setVariablesMessageElement(array('champs' => 'type_email'));
    //$type_email -> setVariablesMessageElement(array('type_email' => 'Veuillez sélectionner un type de mail'));

    $form_message->addElements(array($message, $email, $type_email));


    /* FORMULAIRE COMMANDE */
    //$form = new Form();


    if (count($_POST)) {

        //si on poste le formulaire d'envoi d'emails
        if (isset($_POST['form_email'])) {

            //si form non-valide push errors
            if (!$form_message->isValid($_POST)) {
                $app->mess->addMessageFormate($form_message->getMessagesFormat());
            } else {

                //ctrl emails supp
                $erreurs = null;
                $wrong_emails = array();
                $emails_supp = array();
                if (trim($_POST['emails_sup']) != '') {
                    $emails_supp = explode(';', trim($_POST['emails_sup']));
                    if (count($emails_supp) > 0) {
                        foreach ($emails_supp as $k => $sup) {
                            $emails_supp[$k] = trim($sup);
                            //si il est juste vide on met pas d'erreur mais on le garde pas
                            if (trim($sup) == '') {
                                unset($emails_supp[$k]);
                            } else {
                                if (!checkEmail(trim($sup))) {
                                    $wrong_emails[] = $sup;
                                    $erreurs[] = 'L\'email supplémentaire : "' . trim($sup) . '" n\'est pas valide !';
                                    unset($emails_supp[$k]);
                                }
                            }
                        }
                    }
                }


                
                $detail_cmd = null;
                $files_attachment = array();
                if ($_POST['type_email'] == 'devis' || $_POST['type_email'] == 'devis_commande') {
                    $detail_cmd = new Commande_DetailCommande_View_FacturePdf($id);
                    $detail_cmd->getInfos();
                    if (isset($_POST['sous_loc'])) {
                        $detail_cmd->setSousLoc(true);
                    }
                    $tab_info = $detail_cmd->getInformations();
                } elseif ($_POST['type_email'] == 'contrat') {

                	/// particulier?
 					Loader::loadClass('Commande_DetailCommande');
        			$cmd = new Commande_DetailCommande($id);
			        $cmd->getInfos();
			    	$tab_info = $cmd->getInformations();
			    	$reseaux = preg_split("/[\s,]+/", $tab_info['id_reseaux']);

			    	if(in_array(28,$reseaux) OR in_array(34, $reseaux)){
			    		Loader::loadClass("Commande_DetailCommande_View_ContratParticulierPdf");
			            $detail_cmd = new Commande_DetailCommande_View_ContratParticulierPdf($id);
			            $detail_cmd->getInfos();
			            $tab_info = $detail_cmd->getInformations();

			    	}else{
			    		Loader::loadClass('Commande_DetailCommande_View_ContratPdf');
		                $detail_cmd = new Commande_DetailCommande_View_ContratPdf($id);
		                $detail_cmd->getInfos();
		                if (isset($_POST['sous_loc'])) {
		                    $detail_cmd->setSousLoc(true);
		                }
		                $tab_info = $detail_cmd->getInformations();
			    	}

                } elseif ($_POST['type_email'] == 'documents_complementaires') {

                    //ctrl au moins 1 doc supp
                    //+ doc parmis les docs de cette cde
                    if (empty($_POST['documents'])) {
                        $erreurs[] = 'Vous n\'avez pas sélectionné de document complémentaire !';
                    } else {
                        $mdl_UploadDocs = Model::Load('Upload_UploadDocs');
                        $files_attachment = $mdl_UploadDocs->readDir($id);
                        $files_attachment[] = 'chacha.txt'; //test ajout doc inhexistant
                        $files_attachment = getArraysDiffs($files_attachment, $_POST['documents'])['already'];

                        if (count($files_attachment) == 0) {
                            $erreurs[] = 'Vous n\'avez pas sélectionné de document complémentaire !';
                        }
                    }

                    /* juste pour récup les infos manquantes et passer en sous-loc si coché */
                    Loader::loadClass('Commande_DetailCommande_View_ContratPdf');
                    $detail_cmd = new Commande_DetailCommande_View_ContratPdf($id);
                    $detail_cmd->getInfos();
                    if (isset($_POST['sous_loc'])) {
                        $detail_cmd->setSousLoc(true);
                    }
                    $tab_info = $detail_cmd->getInformations();
                }


                //ajout erreurs emails_supp & docs complémentaires
                if (count($erreurs)) {
                    $app->mess->addMessageFormate($erreurs);
                    $app->mess->setIsError(true);
                } else {

                    $client = new User_Client($tab_info['id_client']);
                    $tab_client = $client->getInformations();
                    $tab_client['code'] = $tab_client['pass'];
                    $detail_cmd->create($tab_client);
                    $detail_cmd->generer_email();

                    //On instancie la classe qui va former l'e-mail
                    Loader::loadClass('Mail_FormatEmail', PATH_CLASSES);
                    $contenuMail = new Mail_FormatEmail();
                    $contenuMail->addTitre('LOC-COURTOISIE - offre n° 000' . $detail_cmd->getId());
                    $contenuMail->addPara($message->getValue());

                    $contenuMail->addSignature($app->text->setValeurs(array($app->user->getInformation('prenom'),
                        $app->user->getInformation('nom'),
                        $app->user->getInformation('email'),
                        $app->user->getInformation('tel')
                    ))->getText('email_signature'));


                    //On envoie l'e-mail
                    /*
		    require_once(PATH_CLASSES.'PHPMailer2/PHPMailerAutoload.php');
		    $mail = defaultMailer();
		    $mail->IsHTML(true);
		    */
                    include(PATH_CLASSES . 'Phpmailer' . DIRECTORY_SEPARATOR . 'class.phpmailer.php');
                    $mail = new PHPMailer();
                    $mail->CharSet = "UTF-8";
                    $mail->IsHTML(true);
                    $mail->From = $app->user->getInformation('email');
                    $mail->FromName = 'LOC-COURTOISIE';
                    $mail->Sender = $app->user->getInformation('email');
                    $mail->SetFrom($app->user->getInformation('email'), 'LOC-COURTOISIE');
                    $mail->ConfirmReadingTo = $app->user->getInformation('email');
                    $mail->addCustomHeader('Return-Receipt-To: ' . $app->user->getInformation('email'));
                    $mail->addCustomHeader('X-Confirm-Reading-To: ' . $app->user->getInformation('email'));
                    $mail->addCustomHeader('Disposition-Notification-To: ' . $app->user->getInformation('email'));

                    $mail->AddAddress($email->getValue());
                    //$mail->AddBCC($app->user->getInformation('email'));

                    //si on a des destinataires multiples on les ajoute en copie cachée
                    if (count($emails_supp) > 0) {
                        foreach ($emails_supp as $BCC) {
                            $mail->AddBCC($BCC);
                        }
                    }

                    $mail->Subject = 'LOC-COURTOISIE - ' . $tab_client['societe'] . ' offre n° 000' . $detail_cmd->getId();
                    $mail->Body .= $contenuMail->create();
                    //ajouter un pixel de confirmation de lecture
                    //setMailPixelReadConfirmation($mail, 'http://www.xxx.com/?uid=xxx');

                    if ($_POST['type_email'] == 'devis') {
                        $infos = array('devis_delivred' => 1);
                        $db->sqlQuery(query_update_prep("commandes", $infos, "id='" . $detail_cmd->getId() . "'"));
                    }

                    if (!in_array($_POST['type_email'], array('devis_commande', 'documents_complementaires'))) {
                        $mail->AddAttachment(PATH_PDF . 'offre_000' . $detail_cmd->getId() . '.pdf');
                    }

                    //Documents complémentaires
                    if ($_POST['type_email'] == 'documents_complementaires') {
                        $directory = DOCS_ORDERS_PATH . DIRECTORY_SEPARATOR . md5($detail_cmd->getId()) . DIRECTORY_SEPARATOR;
                        foreach ($files_attachment as $file) {
                            $mail->AddAttachment($directory . $file);
                        }
                    }

                    //Option Perte Financière doc automatique
                    if (isset($_POST['join_pf'])) {
                        $mail->AddAttachment(DOCS_ORDERS_PATH . DIRECTORY_SEPARATOR . 'default' . DIRECTORY_SEPARATOR . 'BULLETIN-ADHESION-PF.pdf');
                    }

                    if (!$mail->Send()) // la requete ne s'est pas bien passée
                    {
                        //var_dump($mail);
                        echo "pas envoyé";
                        exit;
                    } else // la requete s'est bien déroulée.
                    {
                        //var_dump('SENT');
                        header('location:' . $app->urlFor('order_edit', array('id' => $id)) . '?' . $app->mess->addMessage('prod_info_save')->getMessageUrl());
                        exit;
                    }
                    //var_dump($mail);
                    exit;
                }
            }
        }

        //Traitement du post du formulaire informations véhicule
        if ($form->isValid($_POST) && isset($_POST['update_vehicule'])) {
            //Traitement du formulaire des commandes
            $db = Mysql::getInstance();

            Loader::loadClass('Commande_Commande');
            $order = new Commande_DetailCommande($id_order->getValue());
            $commande = new Commande_Commande($id_order->getValue());
            $order->getInformations();

            $infos = array(
                'id_langue' => 138,
                'total_ht' => $total_ht->getValue() * 1,
                'total_ttc' => $total_ht->getValue() * 1.2,
                'id_client' => $id_client->getValue(),
                'commentaire_pdf' => $commentaire_pdf->getValue(),
                'id_site' => $id_site->getValue(),
                'code' => aleatoire(10) . time(),
                'valide' => 1,
                'km' => $km->getValue(),
                'duree' => $duree->getValue(),
                'km_moyen' => $km_moyen->getValue() != '' ? $km_moyen->getValue() : 0,
                'duree_moyenne' => $duree_moyenne->getValue() != '' ? $duree_moyenne->getValue() : 0,
                'prix_unitaire_ht' => isset($_POST['prix_unitaire_ht']) ? $_POST['prix_unitaire_ht'] : 0,
                'id_type_contrat' => $id_type_contrat->getValue(),
                'periodicite' => $periodicite->getValue()
                //'commentaire_pdf' => $commentaire_pdf->getValue()
            ); 

            $mode = 'edition';
            $zDate = DateTime::createFromFormat('d/m/Y H:i', $date->getValue() . ' ' . date('H:i'));
            if ($id_order->getValue() == null) {
                $mode = 'creation';
                $infos['id_user'] = $app->user->getId();
                $infos['date'] = $zDate->getTimestamp();
                //LAST debug By Niaina 
				//echo query_insert_prep("commandes", $infos);exit;
                $db->sqlQuery(query_insert_prep("commandes", $infos));
                $id = $db->getLastInsertId();
				$mdl_cmd->setCommandComment($id, 'Création devis', $app->user->getInformation('nom'), true);
            } else {
                $infos['modification_date'] = time();
                $db->sqlQuery(query_update_prep("commandes", $infos, "id='" . $id_order->getValue() . "'"));
                $id = $id_order->getValue();
            }

            if (is_array($reseau->getValue()) AND count($reseau->getValue())) {
                $db->sqlQuery("DELETE FROM commandes_reseaux WHERE id_commande='" . $id . "'");
                foreach ($reseau->getValue() as $r) {
                    $datas = array('id_commande' => $id, 'id_reseau' => $r);
                    //var_dump($datas);
                    $mdl_cmd_reseaux->save($datas);
                }
            }

            $last_state = $order->getEtatDernier();
            if ($last_state == null OR $last_state['id_commande_etat'] < $id_etat->getValue()) {
                if ($id_etat->getValue() == 1) {
                    $zDate = DateTime::createFromFormat('d/m/Y H:i:s', $date->getValue() . ' ' . date('H:i:s'));
                    $infos = array('id_commande' => $id,
                        'id_commande_etat' => $id_etat->getValue(),
                        'date' => $zDate->getTimestamp()
                    );
                } else {
                    $infos = array('id_commande' => $id,
                        'id_commande_etat' => $id_etat->getValue(),
                        'date' => time(),
                    );
                    $mdl_cmd->setCommandComment($id_order, 'Dossier à l\'étude', $app->user->getInformation('nom'));
                }
                //$commande->changeEtat($infos);

                $db->sqlQuery(query_update_prep("commandes", array('date' => time()), "id='" . $id_order->getValue() . "'"));
            } else {
                //$db->sqlQuery(query_insert_prep("commandes_etapes_etats", array('date' => $zDate->getTimestamp(), 'id_commande' => $id_order->getValue(), 'id_commande_etat' => $id_etat->getValue())));
            }
            $mdl_cmd->setCommandComment($id, $commentaire->getValue(), $app->user->getInformation('nom'));

            //creation de l'etat
            if ($mode == 'creation') {
                $mdl_cmd_etape_etat = Model::Load('Commande_ModelExtends_EtapeEtat');
                $mdl_cmd_etape_etat->setCommandeEtapeEtat($id, 1, $app->user->getId());
            }
            //Traitement du formulaire véhicule
            Loader::loadClass('Catalogue_Produit');
			$req_new_produit = $db->sqlQuery("SELECT p.id FROM extension_produit ep INNER JOIN produits p ON p.id = ep.produit_id WHERE ep.id = '".$id_produit->getValue()."' LIMIT 1");
			$info_new_produit = $db->sqlAssoc($req_new_produit);

            //$produitInstance = new Catalogue_Produit($info_new_produit['id']);
            $produitInstance = new Catalogue_Produit($id_produit->getValue());
            $produitInstance->getInformations();

            $first_item = null;

            if ($produitInstance !== null AND count($produitInstance->getInformations())) {

                $db = Mysql::getInstance();

                if ($first_item == null) {

                    $req_item = $db->sqlQuery("SELECT max(id) as id FROM commandes_items");
                    $info = $db->sqlAssoc($req_item);

                    $first_item = $info['id'] + 1;
                }


                $totalSup = 0;
                $db->sqlQuery("DELETE FROM commandes_supplements WHERE id_commande='" . vers_base($id) . "'");
                $req_item_old = $db->sqlQuery("SELECT  is_delivred, is_sous_loc, id_financeur, id_concessionnaire,date_env_contrat, date_rec_contrat,	date_livraison_prev, date_livraison, date_retour_prev, date_retour, immatriculation, chassis FROM commandes_items WHERE id_commande='" . vers_base($id) . "' GROUP BY id_commande");
                $infoOldItem = $db->sqlAssoc($req_item_old);

				if ( $mode == 'creation' || $last_state['id_commande_etat'] == 1 )
					$db->sqlQuery("DELETE FROM commandes_items WHERE id_commande='" . vers_base($id) . "'");

                if (is_array($prix_supplements->getValue()) AND count($prix_supplements->getValue())) {
                    $caracts = $caracteristiques_options->getListe();

                    foreach ($prix_supplements->getValue() as $s => $q) {
                        if (isset($caracts[$s]) && $prix_supplements->getValue($s) != '') {
                            $prixSup = (int)$prix_supplements->getValue($s);
                            $totalSup += $prixSup;

                            $infos = array(
                                'id_supplement' => $caracts[$s]->getId(),
                                'id_commande' => $id,
                                'qte' => 1,
                                //'id_item' => $first_item,
                                'nom' => serialize($caracts[$s]->getNom()->val('multi')),
                                'prix_unitaire_ht' => $prixSup,
                                'prix_unitaire_ttc' => $prixSup * 1.2,
                                'total_ht' => $prixSup,
                                'total_ttc' => $prixSup * 1.2,
                            );
                            //echo(query_insert_prep('commandes_supplements', $infos));
                            $db->sqlQuery(query_insert_prep('commandes_supplements', $infos));
                        }
                    }
                }

                //Traitement des check box en suppléments
                if (isset($_POST['perte_financiere'])) {
                    $infos = array(
                        'id_supplement' => 41,
                        'id_commande' => $id,
                        'qte' => 1,
                        //'id_item' => $first_item,
                        'nom' => serialize('Perte Financière'),
                        'prix_unitaire_ht' => 0,
                        'prix_unitaire_ttc' => 0,
                        'total_ht' => 0,
                        'total_ttc' => 0
                    );
                    //N var_dump(query_insert_prep('commandes_supplements', $infos));exit;
                    $db->sqlQuery(query_insert_prep('commandes_supplements', $infos));
                }

                if (isset($_POST['stickage'])) {
                    $infos = array(
                        'id_supplement' => 53,
                        'id_commande' => $id,
                        'qte' => 1,
                        //'id_item' => $first_item,
                        'nom' => serialize('Stickage'),
                        'prix_unitaire_ht' => 0,
                        'prix_unitaire_ttc' => 0,
                        'total_ht' => 0,
                        'total_ttc' => 0
                    );
                    //N var_dump(query_insert_prep('commandes_supplements', $infos));exit;
                    $db->sqlQuery(query_insert_prep('commandes_supplements', $infos));
                }

				if (isset($_POST['point_supp'])) {
                    $infos = array(
                        'id_supplement' => 90,
                        'id_commande' => $id,
                        'qte' => 1,
                        //'id_item' => $first_item,
                        'nom' => serialize('Point Supp'),
                        'prix_unitaire_ht' => 0,
                        'prix_unitaire_ttc' => 0,
                        'total_ht' => 0,
                        'total_ttc' => 0
                    );
                    //var_dump(query_insert_prep('commandes_supplements', $infos));exit;
                    $db->sqlQuery(query_insert_prep('commandes_supplements', $infos));
                }

                //Traitement des champs libres
                //Check Champs libre 1
                $champs_libres_check1 = new Catalogue_Liste_ListeCaracteristique();
                $champs_libres_check1->addFiltre('cn.nom', vers_base($champ_libre_label_1->getValue()), '=');
                $champs_libres_check1->removeFiltre('c.is_show_admin');
                $champs_libres_check1->addFiltre('c.is_show_admin', 0, '=');
                $champs_libres_check1->addFiltre('c.is_champ_libre', 1, '=');
                $champs_libres_check1->getListeInfos();
                $champs_libres_check1->getListe();

                if (count($champs_libres_check1->getListe()) == 0) {
                    //Insertion caracteristique
                    $db->sqlQuery("
                        INSERT INTO `caracteristiques`(
                            `id_caracteristique_type_valeur`, 
                            `id_type`, 
                            `id_caracteristique_unite`, 
                            `is_relation`, 
                            `is_show_admin`, 
                            `position`, 
                            `aff_fiche_produit`, 
                            `is_mensuel`,
                            `is_champ_libre`,
                            `table`,
                            `classe`,
                            `numero_champ_libre`
                            ) 
                        SELECT
                            5,
                            1,
                            0,
                            0,
                            0,
                            max(position)+1,
                            0,
                            0,
                            1,
                            '',
                            '',
                            1
                        FROM caracteristiques
                    ");

                    $id_caracteristique_new = $db->getLastInsertId();

                    //Insertion libellé
                    $db->sqlQuery("INSERT INTO `caracteristiques_noms`(`id_caracteristique`,`id_langue`, `nom`) VALUES (" . vers_base($id_caracteristique_new) . ", 138, '" . vers_base($champ_libre_label_1->getValue()) . "')");

                } else {
                    $champs_libres = new Catalogue_Liste_ListeCaracteristique();
                    $champs_libres->addFiltre('c.is_champ_libre', 1, '=');
                    $champs_libres->addFiltre('cn.nom', vers_base($champ_libre_label_1->getValue()), '=');
                    $champs_libres->getListeInfos();
                    $champs_libres->getListe();
                    $id_caracteristique_new = array_keys($champs_libres->getListe())[0];
                }

                //Insertion liaison produit
                /*$req_liaison_produit = $db->sqlQuery("
                        SELECT 
                          * 
                        FROM 
                          liaisons_caracteristiques_produits 
                        WHERE 
                          id_produit = " . vers_base($id_produit->getValue()) . "
                          AND id_caracteristique = " . vers_base($id_caracteristique_new) . "
                    ");
                if ($db->sqlNumRows($req_liaison_produit) == 0) {
                    $db->sqlQuery("
                        INSERT INTO `liaisons_caracteristiques_produits`(
                            `id_caracteristique`, 
                            `id_produit`, 
                            `is_prix`, 
                            `is_obligatoire`
                            ) 
                        VALUES (
                            $id_caracteristique_new,
                            " . vers_base($id_produit->getValue()) . ",
                            0,
                            0
                        )
                    ");
                }*/
                //Insert Champs libre 1
                if ($champ_libre_valeur_1->getValue() != '') {
                    $infos = array(
                        'id_supplement' => $id_caracteristique_new,
                        'id_commande' => $id,
                        'qte' => 1,
                        //'id_item' => $first_item,
                        'nom' => serialize($champ_libre_label_1->getValue()),
                        'prix_unitaire_ht' => $champ_libre_valeur_1->getValue(),
                        'prix_unitaire_ttc' => $champ_libre_valeur_1->getValue() * 1.2,
                        'total_ht' => $champ_libre_valeur_1->getValue(),
                        'total_ttc' => $champ_libre_valeur_1->getValue() * 1.2,
                    );
                    //var_dump(query_insert_prep('commandes_supplements', $infos));
                    $db->sqlQuery(query_insert_prep('commandes_supplements', $infos));
                }


                //Check Champs libre 2
                $champs_libres_check2 = new Catalogue_Liste_ListeCaracteristique();
                $champs_libres_check2->addFiltre('cn.nom', vers_base($champ_libre_label_2->getValue()), '=');
                $champs_libres_check2->getListeInfos();
                $champs_libres_check2->getListe();

                if (count($champs_libres_check2->getListe()) == 0) {
                    //Insertion caracteristique
                    $db->sqlQuery("
                        INSERT INTO `caracteristiques`(
                            `id_caracteristique_type_valeur`, 
                            `id_type`, 
                            `id_caracteristique_unite`, 
                            `is_relation`, 
                            `is_show_admin`, 
                            `position`, 
                            `aff_fiche_produit`, 
                            `is_mensuel`,
                            `is_champ_libre`,
                            `table`,
                            `classe`,
                            `numero_champ_libre`
                            ) 
                        SELECT
                            5,
                            1,
                            0,
                            0,
                            0,
                            max(position)+1,
                            0,
                            0,
                            1,
                            '',
                            '',
                            2
                        FROM caracteristiques
                    ");

                    $id_caracteristique_new = $db->getLastInsertId();

                    //Insertion libellé
                    $db->sqlQuery("INSERT INTO `caracteristiques_noms`(`id_caracteristique`,`id_langue`, `nom`) VALUES (" . vers_base($id_caracteristique_new) . ", 138, '" . vers_base($champ_libre_label_2->getValue()) . "')");


                } else {
                    $champs_libres = new Catalogue_Liste_ListeCaracteristique();
                    $champs_libres->addFiltre('c.is_champ_libre', 1, '=');
                    $champs_libres->addFiltre('cn.nom', vers_base($champ_libre_label_2->getValue()), '=');
                    $champs_libres->getListeInfos();
                    $champs_libres->getListe();
                    $id_caracteristique_new = array_keys($champs_libres->getListe())[0];
                }
                //Insertion liaison produit
                /*$req_liaison_produit = $db->sqlQuery("
                        SELECT 
                          * 
                        FROM 
                          liaisons_caracteristiques_produits 
                        WHERE 
                          id_produit = " . vers_base($id_produit->getValue()) . "
                          AND id_caracteristique = " . vers_base($id_caracteristique_new) . "
                    ");*/
                /*if ($db->sqlNumRows($req_liaison_produit) == 0) {
                    $db->sqlQuery("
                        INSERT INTO `liaisons_caracteristiques_produits`(
                            `id_caracteristique`, 
                            `id_produit`, 
                            `is_prix`, 
                            `is_obligatoire`
                            ) 
                        VALUES (
                            $id_caracteristique_new,
                            " . vers_base($id_produit->getValue()) . ",
                            0,
                            0
                        )
                    ");
                }*/
                //Insert Champs libre 2
                if ($champ_libre_valeur_2->getValue() != '') {
                    $infos = array(
                        'id_supplement' => $id_caracteristique_new,
                        'id_commande' => $id,
                        'qte' => 1,
                        //'id_item' => $first_item,
                        'nom' => serialize($champ_libre_label_2->getValue()),
                        'prix_unitaire_ht' => $champ_libre_valeur_2->getValue(),
                        'prix_unitaire_ttc' => $champ_libre_valeur_2->getValue() * 1.2,
                        'total_ht' => $champ_libre_valeur_2->getValue(),
                        'total_ttc' => $champ_libre_valeur_2->getValue() * 1.2,
                    );
                    //echo query_insert_prep('commandes_supplements', $infos);exit;
                    $db->sqlQuery(query_insert_prep('commandes_supplements', $infos));
                }


                $nom_format = ($nom->getValue() == null) ? $produitInstance->getDescriptionCourte()->val() : $nom->getValue();

				
				if ( $mode == 'creation' || $last_state['id_commande_etat'] == 1 ) {
					
				$req_item_marque = $db->sqlQuery("SELECT m.name, ep.finition FROM marques m INNER JOIN produits p ON p.fabricant_id = m.id INNER JOIN extension_produit ep ON ep.produit_id = p.id WHERE ep.id = '".$_REQUEST['hid_extension']."' LIMIT 1");
                $lgMarque = $db->sqlAssoc($req_item_marque);
				$zTmpMarque = $lgMarque['name'];
				
				$zTmpMarque = $lgMarque['name'];
				$zTmpFinition = $lgMarque['finition'];
				
                for ($iCount = 0; $iCount < $qte->getValue(); $iCount++) {
                    $infos = array(
                        'id_commande' => vers_base($id),
                        'id_etat' => '1',
                        'qte' => 1,
                        'nom' => strstr($zTmpFinition, $produitInstance->getNom()->val()) ? serialize(array(138 => strtoupper($zTmpMarque.' '.$zTmpFinition))) : serialize(array(138 => strtoupper($zTmpMarque.' '.$produitInstance->getNom()->val() . ' ' . $zTmpFinition))),
                        'version' => $zTmpFinition,
                        'id_couleur' => 25,
                        'couleur_perso' => $couleur_perso->getValue(),
                        'km' => $km->getValue(),
                        'duree' => $duree->getValue(),
                        'km_moyen' => $km_moyen->getValue() != '' ? $km_moyen->getValue() : 0,
                        'duree_moyenne' => $duree_moyenne->getValue() != '' ? $duree_moyenne->getValue() : 0 ,
                        'prix_unitaire_ht' => $prix_unitaire_ht->getValue(),
                        'prix_unitaire_ttc' => $prix_unitaire_ht->getValue(),
                        'total_ht' => ($prix_unitaire_ht->getValue() * $qte->getValue()) + $totalSup,
                        'total_ttc' => (($prix_unitaire_ht->getValue() * $qte->getValue()) + $totalSup) * 1.2,
                        'id_extension' => $_REQUEST['hid_extension'],
                        'id_type_contrat' => $id_type_contrat->getValue(),
						'periodicite' => $periodicite->getValue(),
						'km_actuel' => $km_actuel->getValue(),
                        'item_order' => $iCount + 1,
                        'loyer_achat_ttc' => isset($_POST['loyer_total']) ? $_POST['loyer_total'] : 0
                    );

                    if (is_array($infoOldItem)) {
                        if ($infoOldItem['is_delivred'] != null && $infoOldItem['is_delivred'] != '')
                            $infos['is_delivred'] = $infoOldItem['is_delivred'];
                        if ($infoOldItem['is_sous_loc'] != null && $infoOldItem['is_sous_loc'] != '')
                            $infos['is_sous_loc'] = $infoOldItem['is_sous_loc'];
                        if ($infoOldItem['id_financeur'] != null && $infoOldItem['id_financeur'] != '')
                            $infos['id_financeur'] = $infoOldItem['id_financeur'];
                        if ($infoOldItem['id_concessionnaire'] != null && $infoOldItem['id_concessionnaire'] != '')
                            $infos['id_concessionnaire'] = $infoOldItem['id_concessionnaire'];

                        if ($infoOldItem['date_env_contrat'] != null && $infoOldItem['date_env_contrat'] != '')
                            $infos['date_env_contrat'] = $infoOldItem['date_env_contrat'];
                        if ($infoOldItem['date_rec_contrat'] != null && $infoOldItem['date_rec_contrat'] != '')
                            $infos['date_rec_contrat'] = $infoOldItem['date_rec_contrat'];
                        if ($infoOldItem['date_livraison_prev'] != null && $infoOldItem['date_livraison_prev'] != '')
                            $infos['date_livraison_prev'] = $infoOldItem['date_livraison_prev'];
                        if ($infoOldItem['date_livraison'] != null && $infoOldItem['date_livraison'] != '')
                            $infos['date_livraison'] = $infoOldItem['date_livraison'];
                        if ($infoOldItem['date_retour_prev'] != null && $infoOldItem['date_retour_prev'] != '')
                            $infos['date_retour_prev'] = $infoOldItem['date_retour_prev'];
                        if ($infoOldItem['date_retour'] != null && $infoOldItem['date_retour'] != '')
                            $infos['date_retour'] = $infoOldItem['date_retour'];

                        /*if ($infoOldItem['immatriculation'] != null && $infoOldItem['immatriculation'] != '' && $iCount == 0)
                            $infos['immatriculation'] = $infoOldItem['immatriculation'];
                        if ($infoOldItem['chassis'] != null && $infoOldItem['chassis'] != '' && $iCount == 0)
                            $infos['chassis'] = $infoOldItem['chassis'];*/
                    }
                    //LAST debug NIAINA echo query_insert_prep('commandes_items', $infos);exit;
                    $db->sqlQuery(query_insert_prep('commandes_items', $infos));
                    if ($iCount == 0)
                        $id_item0 = $db->getLastInsertId();
                }
				}
                if (is_int($id_item0)) {
                    $db->sqlQuery(query_update_prep('commandes_supplements', array('id_item' => $id_item0)), "id_commande=" . $id);
                }else{
					$db->sqlQuery(query_update_prep('commandes_supplements', array('id_item' => $first_item)), "id_commande=" . $id);
				}
				if ( $mode != 'creation' || $last_state['id_commande_etat'] == 1 ) {
					$tVehiculeToUpdate = array(
							'couleur_perso' => $couleur_perso->getValue(),
							'km' => $km->getValue(),
							'km_actuel' => $km_actuel->getValue() != '' ? $km_actuel->getValue() : 0,
							'duree' => $duree->getValue(),
							'km_moyen' => $km_moyen->getValue() != '' ? $km_moyen->getValue() : 0,
							'duree_moyenne' => $duree_moyenne->getValue() != '' ? $duree_moyenne->getValue() : 0 ,
							'prix_unitaire_ht' => $prix_unitaire_ht->getValue(),
							'prix_unitaire_ttc' => $prix_unitaire_ht->getValue(),
							'total_ht' => ($prix_unitaire_ht->getValue() * $qte->getValue()) + $totalSup,
							'total_ttc' => (($prix_unitaire_ht->getValue() * $qte->getValue()) + $totalSup) * 1.2,
							'id_type_contrat' => $id_type_contrat->getValue(),
							'periodicite' => $periodicite->getValue()
					);
					$db->sqlQuery(query_update_prep('commandes_items', $tVehiculeToUpdate , "is_avenant = 0 AND id_commande=" . $id));
				}
                header('location:' . $app->urlFor('order_edit', array('id' => $id)) . '?' . $app->mess->addMessage('prod_info_save')->getMessageUrl());
                exit;
            }
        } else {
            $app->mess->addMessageFormate($form->getMessagesFormat());
            if ($id != null) {
                $order = new Commande_DetailCommande($id);
                $order->getInfos();
                $etat = $order->getEtatDernier();

                $client = new User_Client($order->getInformation('id_client'));
                $client->getInformations();

                if (count($order->getReseaux())) {
                    $infos['reseau'] = $order->getReseauxById();
                }

                $form->populateValues($order->getInformations());
                $form->populateValues(array('id_order' => $id, 'reseau' => $order->getReseauxById(), 'id_etat' => $etat['id_commande_etat'], 'date' => date('d/m/Y', $etat['date'])));
                $form_message->populateValues(array('email' => $client->getInformation('email'), 'message' => 'Message au client'));
            }
        }

    } 
    else if ($id != null) {
		$iIdExtension = 0;
        //Récup infos Commande
        $order = new Commande_DetailCommande($id);
        $order->getInfos();
        $etat = $order->getEtatDernier();

        //Récup infos Client
        $client = new User_Client($order->getInformation('id_client'));
        $client->getInformations();

        //Récup infos Réseaux Client
        if (count($order->getReseaux())) {
            $infos['reseau'] = $order->getReseauxById();
        }


        //Récup infos véhicule new
        $order->getInfos();
		
        $tOrder = $order->getInformations();
        $items = $order->getItems();

        $first_item = null;
        if (count($items)) $first_item = array_keys($items)[0];
		$bHasRelocation = null;

        if ($first_item != null) {
			$bHasRelocation = $items[$first_item]['id_concessionnaire'] == '218' ? true : false;
            $form->populateValues(array(
                'id_type_contrat' => $tOrder['id_type_contrat'],
				'periodicite' => $items[$first_item]['periodicite'],
				'km_actuel' => $items[$first_item]['km_actuel'],
                'id_produit' => $items[$first_item]['id_produit'],
                'couleur_perso' => $items[$first_item]['couleur_perso'],
                'duree' => $tOrder['duree'],
                'km_moyen' => $tOrder['km_moyen'],
                'duree_moyenne' => $tOrder['duree_moyenne'],
                'km' => $tOrder['km'],
                'prix_unitaire_ht' => $items[$first_item]['prix_unitaire_ht'],
                'id_order' => $id,

            ));
			
			$iIdExtension	= $items[$first_item]['id_extension'];

            if ($items[$first_item]['nom'] != null) {
                $nomSerialize = unserialize($items[$first_item]['nom']);
                $form->populateValues(array('nom' => $nomSerialize[138]));
            }
            $req_item = $db->sqlQuery("SELECT count(id) as id FROM commandes_items WHERE id_commande = " . vers_base($id));
            $infoitems = $db->sqlAssoc($req_item);

            $qte_vehicule = $infoitems['id'];
            $form->populateValues(array('qte' => $qte_vehicule));
        }

        //Récup info supplements
        $supplements = array();
        $supplementsPrix = array();
//var_dump($first_item);exit;
        if (count($order->getSupplementsByItem($id))) {
            foreach ($order->getSupplementsByItem($id) as $s) {
                $supplements[$s['id_supplement']] = $s['id_supplement'];
                $supplementsPrix[$s['id_supplement']] = $s['total_ht'];
            }
        }

        $form->populateValues(array(
            'supplements' => $supplements,
            'prix_supplements' => $supplementsPrix
        ));

        $tChampsLibre1 = array();
        $tChampsLibre2 = array();

        $order->getInfos('champs_libres');

        if (count($order->getSupplementsByItem($id)) == 1) {

            $lg = $order->getSupplementsByItem($id)[0];
            //if (unserialize($lg['nom']) != 'Stickage')
            //{
                $tChampsLibre1['id'] = $lg['id_supplement'];
                $tChampsLibre1['label'] = unserialize($lg['nom']);
                $tChampsLibre1['val'] = $lg['total_ht'];

                $form->populateValues(array(
                    'champ_libre_label_1' => $tChampsLibre1['label'],
                    'champ_libre_valeur_1' => $tChampsLibre1['val']
                ));
            //}

        }

        if (count($order->getSupplementsByItem($id)) == 2) {

            $lg = $order->getSupplementsByItem($id)[0];
            //if (unserialize($lg['nom']) != 'Stickage')
            //{
                $tChampsLibre1['id'] = $lg['id_supplement'];
                $tChampsLibre1['label'] = unserialize($lg['nom']);
                $tChampsLibre1['val'] = $lg['total_ht'];

                $form->populateValues(array(
                    'champ_libre_label_1' => $tChampsLibre1['label'],
                    'champ_libre_valeur_1' => $tChampsLibre1['val']
                ));
            //}

            $lg = $order->getSupplementsByItem($id)[1];
            if(unserialize($lg['nom']) != 'Stickage')
            {
                $tChampsLibre2['id'] = $lg['id_supplement'];
                $tChampsLibre2['label'] = unserialize($lg['nom']);
                $tChampsLibre2['valeur'] = $lg['total_ht'];

                $form->populateValues(array(
                    'champ_libre_label_2' => $tChampsLibre2['label'],
                    'champ_libre_valeur_2' => $tChampsLibre2['valeur']
                ));
            }

        }

        //Récup Perte Financière et Stickage
        $mdl_cmd = Model::Load('Commande_ModelExtends_Commande');
        $isPerteFinanciere = false;
        if ($id != null)
            $isPerteFinanciere = $mdl_cmd->isPerteFinanciere($id);

        $hasStickage = false;
        if ($id != null)
            $hasStickage = $mdl_cmd->hasStickage($id);
		
		$hasPointSupp = false;
        if ($id != null)
            $hasPointSupp = $mdl_cmd->hasPointSupp($id);

        //push
		//var_dump($order->getInformations());exit;
        $form->populateValues($order->getInformations());
        $form->populateValues(array('id_order' => $id, 'reseau' => $order->getReseauxById(), 'id_etat' => $etat['id_commande_etat'], 'date' => date('d/m/Y', $etat['date'])));
        $form_message->populateValues(array('email' => $client->getInformation('email'), 'message' => 'Message au client'));
    }


    //INFOS VEHICULE(S)
    Loader::loadClass('Catalogue_Liste_ListeProduit', PATH_CLASSES);
	
	if ( $id != null )
		$liste_produit = new Catalogue_Liste_ListeProduit(true, false, false, '0,1,2');
	else
		$liste_produit = new Catalogue_Liste_ListeProduit(true, true, false, '1,2');
    
    $liste_produit->addOrdre('pn.nom', 'ASC');
    //$liste_produit->addFiltre('p.is_actif', true);
    $liste_produit->getListeInfos();
    $liste_produit->getListe();
	
	//var_dump($liste_produit->getListe());exit;

    $concessionnaires = new Catalogue_Liste_ListeConcessionnaire(true);
    $concessionnaires->addOrdre('c.societe', 'ASC');
    $concessionnaires->getListeInfos();
    $concessionnaires->getListe();

    $financeurs = new Catalogue_Liste_ListeFinanceur(true);
    $financeurs->addOrdre('f.societe', 'ASC');
    $financeurs->getListeInfos();
    $financeurs->getListe();

    Loader::loadClass('Catalogue_Liste_ListeCategorie', PATH_CLASSES);
    $liste_categorie = Catalogue_Liste_ListeCategorie::getInstance();
    $liste_categorie->addOrdre('c.id', 'ASC');
    $liste_categorie->getListeInfos();

    $attribut_duree = new Catalogue_Attribut(1);
    $attribut_duree->getInformations();

    $attribut_km = new Catalogue_Attribut(2);
    $attribut_km->getInformations();

    $attribut_type_loc = new Catalogue_Attribut(4);
    $attribut_type_loc->getInformations();
    //N var_dump($attribut_type_loc->getOptions());exit;
    //Récup Perte Financière et Stickage
    $mdl_cmd = Model::Load('Commande_ModelExtends_Commande');
    $isPerteFinanciere = false;
    if ($id != null)
        $isPerteFinanciere = $mdl_cmd->isPerteFinanciere($id);

    $hasStickage = false;
    if ($id != null)
        $hasStickage = $mdl_cmd->hasStickage($id);

	$hasPointSupp = false;
    if ($id != null)
        $hasPointSupp = $mdl_cmd->hasPointSupp($id);


    $datas = array();
    if (isset($_POST['emails_sup']) && $_POST['emails_sup'] != '')
        $datas['emails_sup'] = $_POST['emails_sup'];

    if (isset($client)) {
        $datas['is_bloque'] = $client->getInformation('is_bloque');
    }
    if (isset($client)) {
        $datas['nb_reseau_client'] = count($client->getReseaux());
    }

    if ($order !== null && $order->getInformation('id_user') != 0) {
        $user = new User_User($order->getInformation('id_user'));
        $user->getInformations();

        $nom_user = ' (crée par ' . $user->getInformation('nom') . ')';
    } else {
        $nom_user = '';
    }

    /* Récupération des documents complémentaires */
    $mdl_UploadDocs = Model::Load('Upload_UploadDocs');
    $files = $mdl_UploadDocs->readDir($id);
    $docs = '';
    $joined_docs = '';
    if (count($files)) {
        sort($files);
        foreach ($files as $file) {
            $link = URL_ADMIN . 'order-docs/' . md5($id) . '/' . $file;
            $docs .= '<li><a href="' . $link . '" title="Afficher le document" target="_blank" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-file-o mr-5"></i>' . $file . '</a><i class="fa fa-trash-o del" title="Retirer le document" data-toggle="tooltip" data-placement="top"></i></li>' . PHP_EOL;
            $joined_docs .= '<option value="' . $file . '">' . $file . '</option>' . PHP_EOL;
        }
    }

    //form commande
	//var_dump($form);exit;
    $datas['id'] = $id;
    $datas['form'] = $form;

    //form vehicule
    //$datas['form_vehicule'] = $form_vehicule;
    $datas['liste_produit'] = $liste_produit;
    $datas['attribut_type_loc'] = $attribut_type_loc;

    $datas['isPerteFinanciere'] = $isPerteFinanciere;
    $datas['hasStickage'] = $hasStickage;
    $datas['hasPointSupp'] = $hasPointSupp;

    //form message
    $datas['form_message'] = $form_message;
    $datas['liste_sites'] = $sites;
    $datas['liste_etats'] = $liste_etats;
    $datas['title'] = ($order === null OR $etat == null) ? 'Nouveau devis client' : $etat['etat'] . ' n°' . $id . ' ' . $order->getInformation('client') . $nom_user;
    $datas['back_url'] = ($etat != null AND $etat['id_commande_etat'] == 2) ? $app->urlFor('orders', array('type' => 'order')) : $app->urlFor('orders', array('type' => 'estimate'));
    $datas['order'] = ($order !== null) ? $order : null;
    $datas['liste_reseaux'] = $liste_reseaux;
    $datas['docs'] = $docs;
    $datas['joined_docs'] = $joined_docs;
    $datas['isCmdPF'] = $isCmdPF;
    $datas['new_view'] = true; //(!empty($new_view));
    $datas['bhasrelocation'] = $bHasRelocation; //(!empty($new_view));
    $datas['id_extension'] = $iIdExtension; //(!empty($new_view));

    //Suppléments

    $datas['supplements'] = $caracteristiques_options;

    if ($id !== null) {
        $mdl_cmd_etape_etat = Model::Load('Commande_ModelExtends_EtapeEtat');
        $datas['cmd_status'] = $mdl_cmd_etape_etat->getCommandeEtapeEtat($id)['id_commande_etat'];
    } else {
        $datas['cmd_status'] = 1;
    }

    $app->render('order' . DIRECTORY_SEPARATOR . 'edit_order_new.php', $datas);

})->via('GET', 'POST')->name('order_edit');

$app->get('/order/state/(:id)/(:state)', function ($id, $state) use ($app) {

    $db = Mysql::getInstance();

    Loader::loadClass('Commande_Commande');
    $commande = new Commande_Commande($id);

    if ($state == 2) {
        $db->sqlQuery(query_update_prep("commandes", array('date' => time()), "id='" . $id . "'"));
    }

    $commande->getInformations();

    $infos = array('id_commande' => $id,
        'id_commande_etat' => $state,
        'date' => time()
    );

    if ($commande->changeEtat($infos)) {
        header('location:' . $app->urlFor('orders'));
        exit;
    }

})->name('order_state');

/* OLD VIEW
$app->get('/order/vehicules/(:id)', function ($id = null) use ($app) {

	if($id != null)
	{
		$order = new Commande_DetailCommande($id);
		$order->getInfos();

		$datas = array();
		$datas['order'] = $order;
		$datas['is_ajax'] = true;

		$app->view->useMasterLayout(false);

		if(!isset($_GET['simple']))
			$app->render('vehicule'.DIRECTORY_SEPARATOR.'_list.php',$datas);
		else
			$app->render('vehicule'.DIRECTORY_SEPARATOR.'_list_simple.php',$datas);
	}

})->name('order_vehicules');
*/

$app->get('/order/delete/(:id)', function ($id_cmd) use ($app) {

    $commande = new Commande_Commande();
    $commande->setId($id_cmd);
    $commande->delete();

    $url_parsed = parse_url($_SERVER['HTTP_REFERER']);
    $redir = $url_parsed['scheme'] . '://' . $url_parsed['host'] . $url_parsed['path'] . $_SERVER['QUERY_STRING'];

    header('Location: ' . $_SERVER['HTTP_REFERER'] . '?' . $app->mess->addMessage('order_del')->getMessageUrl());
    exit;

})->name('order_delete');

$app->get('/order/pdf/(:id)', function ($id) use ($app) {

    $app->response->headers->set('Content-Type', "application/pdf");

    if (isset($_GET['devis'])) {

        Loader::loadClass('Commande_DetailCommande_View_FacturePdf');
        $detail_cmd = new Commande_DetailCommande_View_FacturePdf($id);
        $detail_cmd->getInfos();

        $tab_info = $detail_cmd->getInformations();

        $client = new User_Client($tab_info['id_client']);
        $tab_client = $client->getInformations();

        $tab_client['code'] = $tab_client['pass'];
        //ajout d'un logo
        $detail_cmd->create($tab_client);

        $detail_cmd->generer_email();
        $app->response->setBody($detail_cmd->generer_pdf());


    } elseif (isset($_GET['contrat'])) {

        $contrat_courte_duree = false;

        Loader::loadClass('Commande_DetailCommande');
        $cmd = new Commande_DetailCommande($id);
        if (count($cmd->getItems())) {
            $item = current($cmd->getItems());
            $attributs = unserialize($item['attributs_serialize']);
            if (isset($attributs[1]['options'])) {
                $option = current($attributs[1]['options']);
                if (isset($option['nom']) AND (int)$option['nom'] < 12) {
                    $contrat_courte_duree = true;
                }
            }
        }
        
        $cmd->getInfos();
    	$tab_info = $cmd->getInformations();
    	$reseaux = preg_split("/[\s,]+/", $tab_info['id_reseaux']);

    	if(in_array(28,$reseaux) OR in_array(34, $reseaux)){
    		Loader::loadClass("Commande_DetailCommande_View_ContratParticulierPdf");
            $detail_cmd = new Commande_DetailCommande_View_ContratParticulierPdf($id);
    	}else{
    		if ($contrat_courte_duree === false) {
            $detail_cmd = new Commande_DetailCommande_View_ContratPdf($id);
	        } else {
	            $detail_cmd = new Commande_DetailCommande_View_ContratCourteDureePdf($id);
	        }
    	}

      

        $detail_cmd->getInfos();
        $tab_info = $detail_cmd->getInformations();
        $client = new User_Client($tab_info['id_client']);
        $tab_client = $client->getInformations();
        $tab_client['code'] = $tab_client['pass'];

        //ajout d'un logo
        $detail_cmd->create($tab_client);

        $detail_cmd->generer_email();
        $app->response->setBody($detail_cmd->generer_pdf());
    }


})->name('order_pdf');

$app->get('/order/comments/(:id_order)', function ($id_order) use ($app) {

    $mdl_cmd = Model::Load('Commande_ModelExtends_Commande');
    $mdl_cmd_items = Model::Load('Commande_ModelExtends_Item');
    $mdl_cmd->id = $id_order;
    $cmd = $mdl_cmd->read();
    if (!$cmd) {
        throw new Exception('Commande inexistante !');
        exit;
    }

    if (!empty($_POST['commentaire'])) {
        if (trim($_POST['commentaire']) != '') {
            $mdl_cmd->setCommandComment($id_order, $_POST['commentaire'], $app->user->getInformation('nom'));
            $cmd = $mdl_cmd->read();
        }
    }

    $datas = array('cde' => $cmd);
    $datas['is_modal'] = (isset($_GET['ajax'])) ? true : false;
    $datas['title'] = 'Commentaires commande/devis';
    $app->render('order' . DIRECTORY_SEPARATOR . 'comments.php', $datas);
    exit;
})->via('GET', 'POST')->name('order_comments');

//SetItemsSousLoc


$app->get('/order/relaunch/(:id_order)', function ($id_order) use ($app) {

    $mdl_cmd = Model::Load('Commande_ModelExtends_Commande');
    $mdl_cmd_items = Model::Load('Commande_ModelExtends_Item');
    $mdl_cmd->id = $id_order;
    $cmd = $mdl_cmd->read();
    if (!$cmd) {
        throw new Exception('Commande inexistante !');
        exit;
    }


    $success = false;
    $erreurs = null;
    $mail_ctx = 'Bonjour,<br/><br/>

Nous vous avons envoyé à l’étude la commande de <strong> {{customer_name}} </strong> pour <strong>{{nb_vehicules}}  {{modeles+finitions}} </strong>, pour laquelle nous n’avons pas de retour.<br/><br/> 
Merci de nous informer de l’issue de cette étude dans les plus brefs délais. <br/><br/>

Bien à vous,

LEASEWAY<br/><br/><hr/>

<img src="' . URL_SITE . 'images/mails/header.jpg">';

    $customer = $mdl_cmd->getCommandCustomer($cmd['id']);
    $cmd_items = $mdl_cmd->getCommandItems($cmd['id']);


    $items_names = '';

    if (count($cmd_items))
        $items_names = ($mdl_cmd_items->getItemName($cmd_items[0]['id']));

    //die('okok');
    $mail_ctx = str_replace(array('{{customer_name}}', '{{nb_vehicules}}', '{{modeles+finitions}}'),
        array($customer['societe'], count($cmd_items), $items_names), $mail_ctx);
    //ctrl form
    if (count($_POST)) {

        $_POST['emails'] = preg_replace('/\s/', '', $_POST['emails']);

        //ctrl emails
        $wrong_emails = array();
        $emails = array();
        if ($_POST['emails'] != '') {
            $emails = explode(';', trim($_POST['emails']));
            if (count($emails) > 0) {
                foreach ($emails as $k => $sup) {
                    $emails[$k] = trim($sup);
                    //si il est juste vide on met pas d'erreur mais on le garde pas
                    if (trim($sup) == '') {
                        unset($emails[$k]);
                    } else {
                        if (!checkEmail(trim($sup))) {
                            $wrong_emails[] = $sup;
                            $erreurs[] = 'L\'email : "' . trim($sup) . '" n\'est pas valide !';
                            unset($emails[$k]);
                        }
                    }
                }
            }
        }

        if (count($emails) < 1) {
            $erreurs[] = 'Aucun email n\'a été sélectionné';
        }

        if ($erreurs == null) {



            include_once(PATH_CLASSES . 'PHPMailer3' . DIRECTORY_SEPARATOR . 'PHPMailerAutoload.php');
            $mail = new PHPMailer();
            $mail->CharSet = "UTF-8";
            $mail->Encoding = "quoted-printable";
            $mail->IsHTML(true);

            if ($_SERVER['REMOTE_ADDR'] != '5.1.33.16') {
                $mail->AddAddress($emails[0]);
                for ($i = 1; $i < count($emails); $i++) {
                    $mail->AddBCC($emails[$i]);
                }
            } else {
                $mail->AddAddress('aurelien.espaiweb@gmail.com');
            }

            $mail->From = $app->user->getInformation('email');
            $mail->FromName = NOM_SITE;
            $mail->Subject = NOM_SITE . ' - retour d\'étude ' . $cmd['id'];
            $mail->Body = $mail_ctx;

            if ($mail->send()) {
                $success = '<div class="alert alert-success m-5 pd-5">Le ' . $_POST['relaunch'] . ' a bien été relancé</div>';

                $mdl_cmd->setCommandComment($id_order, 'Relance du ' . $_POST['relaunch'] . ' pour le retour d\'étude', $app->user->getInformation('nom'));

            } else {
                $erreurs[] = 'Le mail n\'a pu être envoyé, contactez votre administrateur';
            }

        }

        if (count($erreurs)) {
            $erreurs = span_erreur($erreurs, '<br/>');
        }

    }
    $objet = NOM_SITE . ' - retour d\'étude ' . $cmd['id'];

    //var_dump(html_entity_decode($mail_ctx)); die;
    $datas = array('cmd' => $cmd, 'message_mail' => $mail_ctx, 'objet' => $objet);
    $datas['is_modal'] = (isset($_GET['ajax'])) ? true : false;
    $datas['title'] = 'Relancer le Concessionnaire ou Loueur pour Étude';
    $datas['success'] = $success;
    $datas['errors'] = $erreurs;
    $app->render('order' . DIRECTORY_SEPARATOR . 'relaunches_study.php', $datas);
    exit;
})->via('GET', 'POST')->name('order_relaunch');

$app->get('/order/relaunch-suivi-etude/(:id_order)', function ($id_order) use ($app) {

    $mdl_cmd = Model::Load('Commande_ModelExtends_Commande');
    $mdl_cmd_items = Model::Load('Commande_ModelExtends_Item');
    $mdl_cmd->id = $id_order;
    $cmd = $mdl_cmd->read();
    if (!$cmd) {
        throw new Exception('Commande inexistante !');
        exit;
    }


    $success = false;
    $erreurs = null;
    $mail_ctx = 'Bonjour,<br/><br/>

Nous vous avons envoyé à l’étude la commande de <strong> {{customer_name}} </strong> pour <strong>{{nb_vehicules}}  {{modeles+finitions}} </strong>, pour laquelle nous n’avons pas de retour.<br/><br/> 
Merci de nous informer de l’issue de cette étude dans les plus brefs délais. <br/><br/>

Bien à vous, <br>

LEASEWAY<br/><br/><hr/>

<img src="' . URL_SITE . 'images/mails/header.jpg">';

    $mail_modify = '
Bonjour,{{br}}{{br}}

Nous vous avons envoyé à l’étude la commande de {{strong}} {{customer_name}}{{strongClose}} pour{{strong}}{{nb_vehicules}}  {{modeles+finitions}} {{strongClose}}, pour laquelle nous n’avons pas de retour.{{br}} {{br}}
Merci de nous informer de l’issue de cette étude dans les plus brefs délais.{{br}} {{br}} 

Bien à vous, {{br}}

LEASEWAY{{br}}{{br}}{{br}} {{hr}}

{{imgLeasyWay}}

';

    $customer = $mdl_cmd->getCommandCustomer($cmd['id']);
    //$customer = $mdl_cmd->getCommandCustomerSameGroup($cmd['id']);
    $cmd_items = $mdl_cmd->getCommandItems($cmd['id']);


    $items_names = '';

    if (count($cmd_items))
        $items_names = ($mdl_cmd_items->getItemName($cmd_items[0]['id']));

    //die('okok');
    $mail_ctx = str_replace(array('{{customer_name}}', '{{nb_vehicules}}', '{{modeles+finitions}}'),
        array($customer['societe'], count($cmd_items), $items_names), $mail_ctx);
    $mail_modify = str_replace(array('{{customer_name}}', '{{nb_vehicules}}', '{{modeles+finitions}}', '{{br}}', '{{strong}}', '{{strongClose}}', '{{hr}}', '{{imgLeasyWay}}'),
        array($customer['societe'], count($cmd_items), $items_names, ' ', ' ', ' ', ' ', ''), $mail_modify);
    //ctrl form

    if (count($_POST)) {

        $_POST['emails'] = preg_replace('/\s/', '', $_POST['emails']);

        //ctrl emails
        $wrong_emails = array();
        $emails = array();
        if ($_POST['emails'] != '') {
            $emails = explode(';', trim($_POST['emails']));
            if (count($emails) > 0) {
                foreach ($emails as $k => $sup) {
                    $emails[$k] = trim($sup);
                    //si il est juste vide on met pas d'erreur mais on le garde pas
                    if (trim($sup) == '') {
                        unset($emails[$k]);
                    } else {
                        if (!checkEmail(trim($sup))) {
                            $wrong_emails[] = $sup;
                            $erreurs[] = 'L\'email : "' . trim($sup) . '" n\'est pas valide !';
                            unset($emails[$k]);
                        }
                    }
                }
            }
        }

        if (count($emails) < 1) {
            $erreurs[] = 'Aucun email n\'a été sélectionné';
        }

        if ($erreurs == null) {
            $mail_modify_for_mail = "";

            if (isset($_POST['message_mail']) && $_POST['message_mail'] != '') {
                $mail_modify_for_mail .= nl2br($_POST['message_mail']);
            } else {
                $mail_modify_for_mail .= "
Nous vous avons envoyé à l’étude la commande de {{strong}} {{customer_name}}{{strongClose}} pour{{strong}}{{nb_vehicules}}  {{modeles+finitions}} {{strongClose}}, pour laquelle nous n’avons pas de retour.{{br}} {{br}}
Merci de nous informer de l’issue de cette étude dans les plus brefs délais.{{br}} {{br}} ";

            }


            $mail_modify_for_mail .= "
 {{hr}}

{{imgLeasyWay}}
";

            $mail_modify_for_mail = str_replace(array('{{customer_name}}', '{{nb_vehicules}}', '{{modeles+finitions}}', '{{br}}', '{{strong}}', '{{strongClose}}', '{{hr}}', '{{imgLeasyWay}}'),
                array($customer['societe'], count($cmd_items), $items_names, '<br/>', '<strong>', '</strong>', '<hr>', '<img src="' . URL_SITE . 'images/mails/header.jpg">'), $mail_modify_for_mail);


            include_once(PATH_CLASSES . 'PHPMailer3' . DIRECTORY_SEPARATOR . 'PHPMailerAutoload.php');
            $mail = new PHPMailer();
            $mail->CharSet = "UTF-8";
            $mail->Encoding = "quoted-printable";
            $mail->IsHTML(true);

            if ($_SERVER['REMOTE_ADDR'] != '5.1.33.16') {
                $mail->AddAddress($emails[0]);
                for ($i = 1; $i < count($emails); $i++) {
                    $mail->AddBCC($emails[$i]);
                }
            } else {
                $mail->AddAddress('aurelien.espaiweb@gmail.com');
            }

            $mail->From = $app->user->getInformation('email');
            $mail->FromName = NOM_SITE;
            if (isset($_POST['objet_mail']) && $_POST['objet_mail'] != '') {
                $mail->Subject = $_POST['objet_mail'];
            } else {
                $mail->Subject = NOM_SITE . ' - retour d\'étude ' . $cmd['id'];
            }
            $mail->Body = $mail_modify_for_mail;

            if ($mail->send()) {
                $success = '<div class="alert alert-success m-5 pd-5">Le ' . $_POST['relaunch'] . ' a bien été relancé</div>';

                $mdl_cmd->setCommandComment($id_order, 'Relance du ' . $_POST['relaunch'] . ' pour le retour d\'étude', $app->user->getInformation('nom'));

            } else {
                $erreurs[] = 'Le mail n\'a pu être envoyé, contactez votre administrateur';
            }

        }

        if (count($erreurs)) {
            $erreurs = span_erreur($erreurs, '<br/>');
        }

    }
    $objet = NOM_SITE . ' - retour d\'étude ' . $cmd['id'];

    //var_dump(html_entity_decode($mail_ctx)); die;
    $datas = array('cmd' => $cmd, 'message_mail' => $mail_modify, 'objet' => $objet);
    $datas['is_modal'] = (isset($_GET['ajax'])) ? true : false;
    $datas['title'] = 'Relancer le Concessionnaire ou Loueur pour Étude';
    $datas['success'] = $success;
    $datas['errors'] = $erreurs;
    $app->render('order' . DIRECTORY_SEPARATOR . 'relaunches_study.php', $datas);
    exit;
})->via('GET', 'POST')->name('order_relaunch_suivi_etude');

$app->get('/order/relaunch-customer/(:id_item)', function ($id_item) use ($app) {

    $mdl_cmd = Model::Load('Commande_ModelExtends_Commande');
    $mdl_cmd_items = Model::Load('Commande_ModelExtends_Item');
    $mdl_cmd_items->id = $id_item;

    $cmd_item = $mdl_cmd_items->read();
    if (!$cmd_item) {
        throw new Exception('Véhicule commandé inexistant !');
        exit;
    } else {
        //var_dump($cmd_item['id']); die;
        $customer = $mdl_cmd_items->getCommandeFromItemId($cmd_item['id']);
        //$customer = $mdl_cmd->getCommandCustomer($cmd_item['id'], 'societe, TRIM(CONCAT(nom, " ", prenom)) AS customer_contact, email');
    }

    $message_mail = 'Madame, Monsieur,
    
Nous vous avons envoyé le {{date_env_contrat}} votre contrat de location pour  {{nb_vehicules}} {{modele_finition}}, à ce jour, et sauf erreur de notre part, nous n’avons pas eu de retour de votre part.

Merci de faire le nécessaire rapidement afin que la commande puisse être confirmée au concessionnaire. 

Bien à vous, 

LEASEWAY';


    $message_mail = str_replace(array(
        '{{date_env_contrat}}',
        '{{nb_vehicules}}',
        '{{modele_finition}}',
    ),
        array(
            date('d/m/Y', $cmd_item['date_env_contrat']),
            $mdl_cmd_items->getNbItems($cmd_item['id_commande']),
            $mdl_cmd_items->getItemName($cmd_item['id']),
        ), $message_mail);

    $success = false;
    $erreurs = null;

    //ctrl form
    if (count($_POST)) {

        //ctrl message
        if (!strlen(trim(strip_tags(@$_POST['message_mail']))) > 10) {
            $erreurs[] = 'le massage doit faire au moins 10 caractères';
        } else {
            $mail_ctx = nl2br(trim(strip_tags(@$_POST['message_mail']))) . '<br/><br/><hr/><img src="' . URL_SITE . 'images/mails/header.jpg">';
        }

        if (!count($erreurs)) {
            $_POST['emails'] = preg_replace('/\s/', '', $_POST['emails']);

            //ctrl emails
            $wrong_emails = array();
            $emails = array();
            if ($_POST['emails'] != '') {
                $emails = explode(';', trim($_POST['emails']));
                if (count($emails) > 0) {
                    foreach ($emails as $k => $sup) {
                        $emails[$k] = trim($sup);
                        //si il est juste vide on met pas d'erreur mais on le garde pas
                        if (trim($sup) == '') {
                            unset($emails[$k]);
                        } else {
                            if (!checkEmail(trim($sup))) {
                                $wrong_emails[] = $sup;
                                $erreurs[] = 'L\'email : "' . trim($sup) . '" n\'est pas valide !';
                                unset($emails[$k]);
                            }
                        }
                    }
                }
            }
        }


        if (!count($erreurs)) {
            //si on on envoie le mail + ajout commentaire

            include_once(PATH_CLASSES . 'PHPMailer3' . DIRECTORY_SEPARATOR . 'PHPMailerAutoload.php');
            $mail = new PHPMailer();
            $mail->CharSet = "UTF-8";
            $mail->Encoding = "quoted-printable";
            $mail->IsHTML(true);

            if ($_SERVER['REMOTE_ADDR'] != '5.1.33.16') {
                $mail->AddAddress($emails[0]);
                for ($i = 1; $i < count($emails); $i++) {
                    $mail->AddBCC($emails[$i]);
                }
            } else {
                $mail->AddAddress('aurelien.espaiweb@gmail.com');
            }

            $mail->From = $app->user->getInformation('email');
            $mail->FromName = NOM_SITE;
            if (isset($_POST['objet_mail']) && $_POST['objet_mail'] != '')
                $mail->Subject = $_POST['objet_mail'];
            else
                $mail->Subject = NOM_SITE . ' - retour de contrat - ' . $cmd_item['id_commande'];

            $mail->Body = $mail_ctx;

            if ($mail->send()) {
                $success = '<div class="alert alert-success m-5 pd-5">Le Client a bien été relancé</div>';
                $mdl_cmd->setCommandComment($cmd_item['id_commande'], 'Relance client pour réception contrat', $app->user->getInformation('nom'));
            } else {
                $erreurs[] = 'Le mail n\'a pu être envoyé, contactez votre administrateur';
            }
        }


        if (count($erreurs)) {
            $erreurs = span_erreur($erreurs, '<br/>');
        }

    }
    $objet = NOM_SITE . ' - retour de contrat - ' . $cmd_item['id_commande'];

    $datas = array('cmd_item' => $cmd_item, 'customer' => $customer, 'message_mail' => $message_mail, 'objet' => $objet);
    $datas['is_modal'] = (isset($_GET['ajax'])) ? true : false;
    $datas['title'] = 'Relancer le client';
    $datas['success'] = $success;
    $datas['errors'] = $erreurs;
    $app->render('order' . DIRECTORY_SEPARATOR . 'relaunches_customers.php', $datas);
    exit;
})->via('GET', 'POST')->name('order_relaunch_customer');

$app->get('/order/relaunch-customer-suvi-restistution/(:id_item)/(:relance)', function ($id_item, $relance) use ($app) {

    $mdl_cmd = Model::Load('Commande_ModelExtends_Commande');
    $mdl_cmd_items = Model::Load('Commande_ModelExtends_Item');
    $mdl_cmd_items->id = $id_item;

    $cmd_item = $mdl_cmd_items->read();
    if (!$cmd_item) {
        throw new Exception('Véhicule commandé inexistant !');
        exit;
    } else {
        //var_dump($cmd_item['id']); die;
        $customer = $mdl_cmd_items->getCommandeFromItemId($cmd_item['id']);
        //$customer = $mdl_cmd->getCommandCustomer($cmd_item['id'], 'societe, TRIM(CONCAT(nom, " ", prenom)) AS customer_contact, email');
    }
    if ($relance == 0) {
        $message_mail = "
Madame, Monsieur,
     
Vous nous avez accordé votre confiance en souscrivant un contrat de location longue durée pour votre véhicule immatriculé : {{immatriculation}}
qui arrive bientôt à échéance le : {{date_retour_prev}}                        
 
                                                                                                                                                                                                                                           
Par sécurité nous vous conseillons de vérifier également la date de fin de contrat pour éviter toutes erreurs.
 
Afin de préparer au mieux le renouvellement de votre contrat et compte tenu des délais de livraison, je vous invite à prendre contact avec le Service Commercial au 01.47.76.74.07.

Vous aurez la possibilité de conserver votre véhicule le temps que le prochain vous soit livré.  

Merci de revenir vers nous afin de nous indiquer que la procédure de restitution est en cours.

Cordialement,";


        $message_mail = str_replace(array(
            '{{date_retour_prev}}',
            '{{immatriculation}}',

        ),
            array(
                date('d/m/Y', $mdl_cmd_items->date_retour_prev),
                $mdl_cmd_items->immatriculation,
            ), $message_mail);


    } else if ($relance == 1) {
        $message_mail = "
Madame, Monsieur,

Nous vous avons envoyé le {{date_env_restitution}} la procédure de restitution pour votre {{modeleFinition}} immatriculé {{immatriculation}}

Pourriez-vous nous indiquer si la procédure est bien en cours et si vous souhaitez être rappelé par notre service commercial pour le renouvellement de ce(s) véhicule(s).

Bien à vous

LEASEWAY
";
        $message_mail = str_replace(array(
            '{{date_env_restitution}}',
            '{{modeleFinition}}',
            '{{immatriculation}}'

        ),
            array(
                date('d/m/Y', $mdl_cmd_items->date_retour),
                $mdl_cmd_items->getItemName($cmd_item['id']),
                $mdl_cmd_items->immatriculation,
            ), $message_mail);
    }

    /* $message_mail = 'Madame, Monsieur,

 Nous vous avons envoyé le {{date_env_contrat}} votre contrat de location pour  {{nb_vehicules}} {{modele_finition}}, à ce jour, et sauf erreur de notre part, nous n’avons pas eu de retour de votre part.

 Merci de faire le nécessaire rapidement afin que la commande puisse être confirmée au concessionnaire.

 Bien à vous,

 LEASEWAY';*/

    //var_dump($mdl_cmd_items->date_retour_prev); die;


    $success = false;
    $erreurs = null;

    //ctrl form
    if (count($_POST)) {

        //ctrl message
        if (!strlen(trim(strip_tags(@$_POST['message_mail']))) > 10) {
            $erreurs[] = 'le massage doit faire au moins 10 caractères';
      